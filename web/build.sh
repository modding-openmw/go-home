#!/usr/bin/env bash
set -euo pipefail

#
# This script builds the project website. It downloads soupault as needed and
# then runs it, the built website can be found at: web/build
#

this_dir=$(realpath "$(dirname "${0}")")
cd "${this_dir}"

soupault_version=4.11.0
soupault_pkg=soupault-${soupault_version}-linux-x86_64.tar.gz
soupault_path=./soupault-${soupault_version}-linux-x86_64

shas=https://github.com/PataphysicalSociety/soupault/releases/download/${soupault_version}/sha256sums
spdl=https://github.com/PataphysicalSociety/soupault/releases/download/${soupault_version}/${soupault_pkg}

if ! [ -f ${soupault_path}/soupault ]; then
    curl -sLO ${shas}
    curl -sLO ${spdl}
    tar xf ${soupault_pkg}
    grep 'linux-x86_64' sha256sums | sha256sum -c -
fi

echo "Releases without a download link can be downloaded as a dev build from the link above." > site/changelog.md
grep -v "## Go Home!" ../CHANGELOG.md >> site/changelog.md
echo '<div class="center"><video poster="/go-home/img/GoHome.png" controls src="https://static.modding-openmw.com/GoHome.mp4" /></div>' > site/index.md
echo '<div class="center"><a href="https://www.youtube.com/watch?v=ZJK_4OvO_CY">Watch this video on YouTube</a></div>' >> site/index.md
grep -v "# Go Home!" ../README.md >> site/index.md

mkdir site/locking-doors
pandoc ../README-LockingDoors.org > site/locking-doors/index.html

PATH=${soupault_path}:$PATH soupault "$@"
