# Report A Problem Or Request A Feature

Fill out the part that applies to your request and delete the part that does not.

## Report A Problem

If you've found a problem you can help solve it by providing relevant information.

* What version of the mod are you using (found in the `version.txt` file)?
* Try to describe how you get to the problem state so that others can recreate it if possible.
* If the problem relates to a specific NPC or NPCs being bugged, try to gather information about them while in game using [these steps](https://modding-openmw.gitlab.io/go-home/#use-the-lua-console-to-check-an-npc-in-game).
* If the problem relates to this mod breaking a quest, please provide as much detail as possible including:
    * Journal ID for the quest
    * RecordID of any affected NPCs
    * Related journal index ranges to ignore the affected NPCs during

## Request A Feature

If you have an idea for a new feature, please check out [the current planned features list](https://modding-openmw.gitlab.io/go-home/#planned-features).

If your idea isn't mentioned there, please describe it here in as much detail as you need.
