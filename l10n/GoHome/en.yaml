name: "Go Home!"
description: |
  Gives NPCs a schedule, they will go home at night or during bad weather. Highly configurable.

  Please see the included README.md file for more information about the mod.

autoFactoryResetForUpdate: "A Go Home! mod update was detected, a factory reset is being automatically executed to apply changes."
factoryResetAdvised: "A factory reset is required for this change to take effect! (ESC >> Options >> Scripts >> Go Home!)"
needNewerOpenMW: "This mod requires OpenMW 0.49.0 or newer, please update."
dontLoadNPCScheduleEsp: "Go Home! is incompatible with the old NPC Schedule mod"
needOmwAddon: "The 'go-home.omwaddon' file was not found. It is required to use Go Home! The mod cannot be started."
noEnAndFr: "You are running both the English (go-home.omwscripts) and French language plugins (go-home-fr.omwscripts), please only select one!"

modSettingsTitle: "Mod Settings"
modSettingsDesc: "Configure or disable various parameters"

disableOnly_name: "Disable Only"
disableOnly_desc: |
  Unfortunately, the approach taken to determine if an NPC can "go home" causes save files to increase in size because of the need to read cells in order to have a place for the NPC to go to.

  Set this to "Yes" to force all NPCs to simply be disabled, which will avoid the excess save file growth.

  Default: No

minDisableDist_name: "Minimum disable/enable distance"
minDisableDist_desc: |
  The minimum distance an NPC must be away from the player before being disabled or re-enabled. Set to 0 to disable, meaning NPCs will disable or re-enable as soon as they are out of the player's view.

  Default: 682 (1/12th cell)
  Min: 0
  Max: 8192 (one cell)

hourAM_name: "Hour that NPCs come outside"
hourAM_desc: |
  Set the hour in the morning that NPCs come outside or get re-enabled if they lack a home.

  Default: 8.5
  Min: 1
  Max: 12

hourPM_name: "Hour that NPCs go inside"
hourPM_desc: |
  Set the hour in the evening that NPCs go inside or get disabled if they lack a home.

  Default: 21.5
  Min: 12
  Max: 23

goHomeForBadWeather_name: "NPCs go home for bad weather"
goHomeForBadWeather_desc: |
  If enabled NPCs will go home (or be disabled if they don't actually have a proper home) during inclement weather:

    * Rain
    * Thunderstorms
    * Ash Storms
    * Blight Storms
    * Blizzard

  Default: Yes

chanceNoDisable_name: "Rare chance to not disable at night"
chanceNoDisable_desc: |
  If enabled there will be a chance that NPCs without a home will not be disabled at night or during inclement weather (if enabled). Please see the number options below to configure the chance itself.

  Default: Yes

chanceNoDisableNum_name: "No Disable Chance Number"
chanceNoDisableNum_desc: |
  If "Rare chance to not disable at night" is enabled: this number will affect the chance an NPC has to not be disabled at night or during bad weather (if that feature is enabled).

  Higher values mean less chance. There will be a one in N chance the disable happens, where N is the value you set here.

  Default: 10
  Min: 1
  Max: 500

merchantChanceNoDisableNum_name: "Merchant No Disable Chance Number"
merchantChanceNoDisableNum_desc: |
  Same as above but for merchants.

  Higher values mean less chance. There will be a one in N chance the disable happens, where N is the value you set here.

  Default: 5
  Min: 1
  Max: 500

chanceNoDisableAddWeather_name: "Additional No Disable Chance Number For Bad Weather"
chanceNoDisableAddWeather_desc: |
  Higher values mean less chance. This is an additional number added to the above when weather is bad for a smaller chance the NPC will not be disabled.

  The equation looks like:
  Normal Chance Number + Weather Number Set Here = Total Chance Number

  Default: 5
  Min: 0
  Max: 500

useRenderingRaycast_name: "Use a rendering raycast to disable"
useRenderingRaycast_desc: |
  The default behavior is to not disable an NPC if they are in the player's camera viewing range, meaning that they will still be "seen" if a building stands between them and the player.

  When this option is set to "Yes", a rendering raycast is used instead which may be more accurate in that it tries to detect actual line-of-sight hits. It may be more computationally expensive.

  Default: No

debugOn_name: "Enable debug messages in the log"
debugOn_desc: |
  This will print a lot of information about virtually everything this mod does. Spam!

  Default: No

veryDebugOn_name: "Enable very verbose debug messages in the log"
veryDebugOn_desc: |
  This will print a lot LOT of information about virtually everything this mod does. Very spammy!

  Default: No

delaySettingsTitle: "Delay Settings"
delaySettingsDesc: "Configure or disable various delay timings"

maxDelay_name: "Max delay before going inside"
maxDelay_desc: |
  Maximum delay in seconds before an NPC goes inside when conditions are right to do so.

  Default: 30
  Min: 1
  Max: 120

maxOutDelay_name: "Max delay before going outside"
maxOutDelay_desc: |
  Maximum delay in seconds before an NPC comes outside (or is re-enabled) when conditions are right to do so.

  Default: 10
  Min: 0
  Max: 120

initialWeatherDelay_name: "Initial weather monitoring delay"
initialWeatherDelay_desc: |
  Initial delay in seconds before the weather monitor kicks in.

  Default: 60
  Min: 1
  Max: 300

rareLongDelay_name: "Rare chance for longer go inside delay"
rareLongDelay_desc: |
  If enabled there will be a chance that an NPC with a home to go to will have a very long delay before doing so (it adds an extra five real world minutes to any existing delay they may have).

  Default: Yes

timingSettingsTitle: "Timing Frequency Settings"
timingSettingsDesc: "Configure or disable various delay timings"

weatherMonitorFreq_name: "Weather check frequency"
weatherMonitorFreq_desc: |
  Frequency in seconds that the current weather status should be checked for and broadcast to all nearby NPCs.

  Default: 30
  Min: 1
  Max: 300

updateFreq_name: "NPC status update frequency"
updateFreq_desc: |
  Frequency in seconds that NPCs status should be updated. This affects AI actions, too long of an interval will result in awkward pauses before NPCs "walk inside" and possibly other things.

  Please note that you'll have to use the Reset button above to restore the default if you change this! The value cannot be set to a floating point (decimal) number via this settings box at this time. If you want to set this to some float value then you'll have to do it via a Lua script.

  Default: 0.5
  Min: 0.1
  Max: 10

factoryResetSettingsTitle: "Factory Reset And On/Off Switch"
factoryResetSettingsDesc: "Disable the mod and revert all changes made by it, or re-enable it"

factoryReset_name: "Factory Reset"
factoryReset_desc: |
  Revert all changes made by this mod and disable its functionality.

  When "Yes": This mod's functionality is disabled and any changes made by it (e.g., NPC position and/or AI package) are reverted to what they were when this mod first took control of their behavior.

  When "No": The mod runs normally, affecting NPCs. Please note that when you disable then re-enable this mod, you will have to exit and then re-enter any cell you're already in so that NPCs there can be affected by this mod.

factoryResetGo: "Go Home! mod factory reset has been initiated! The mod has also been disabled."
factoryResetOff: "Go Home! has been re-enabled!"
