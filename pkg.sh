#!/bin/sh
set -e

modname=go-home
file_name=$modname.zip

cat > $modname-metadata.toml <<EOF
[package]
name = "Go Home!"
description = "Gives NPCs a schedule, they will go home at night or during bad weather. Highly configurable."
homepage = "https://modding-openmw.gitlab.io/go-home/"
authors = [
	"johnnyhostile",
]
version = "$(git describe --tags --always)"
EOF

cat > $modname-locking-doors-metadata.toml <<EOF
[package]
name = "Go Home! Locking Doors"
description = "Locks doors during night hours, unlocks them during daytime. Highly configurable."
homepage = "https://modding-openmw.gitlab.io/go-home/locking-doors/"
authors = [
	"johnnyhostile",
]
version = "$(git describe --tags --always)"
EOF

zip --must-match --recurse-paths ${file_name} \
    CHANGELOG.md \
    LICENSE \
    README.md \
    README-LockingDoors.org \
    $modname.omwaddon \
    $modname.omwscripts \
    $modname-locking-doors.omwscripts \
    $modname-fr.omwscripts \
    $modname-locking-doors-fr.omwscripts \
    l10n \
    scripts \
    $modname-metadata.toml \
    $modname-locking-doors-metadata.toml

sha256sum ${file_name} > ${file_name}.sha256sum.txt
sha512sum ${file_name} > ${file_name}.sha512sum.txt
