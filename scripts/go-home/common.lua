local async = require("openmw.async")
local core = require("openmw.core")
local storage = require("openmw.storage")
local MOD_ID = "GoHome"
local modSettings = storage.globalSection("SettingsAMod" .. MOD_ID)
local delaySettings = storage.globalSection("SettingsBDelay" .. MOD_ID)

local badWeather = {
    [4] = true,
    [5] = true,
    [6] = true,
    [7] = true,
    -- [8] = true, --TODO: Only NPCs with weak frost resistance go in for snow
    [9] = true
}

local function gameHour()
    return (core.getGameTime() / 60 / 60) % 24
end

local function gridToStr(x, y)
    return string.format("%d_%d", x, y)
end

local function isDaytime(now, hourAM, hourPM)
    return now < hourPM and now >= hourAM
end

local function isNighttime(now, hourAM, hourPM)
    return now < hourAM or now >= hourPM
end

local MIN_DELAY = 1
local function randDelay()
    return math.random(MIN_DELAY, delaySettings:get("maxDelay"))
end
local function randOutDelay()
    return math.random(0, delaySettings:get("maxOutDelay"))
end

local debugOn = modSettings:get("debugOn")
local veryDebugOn = modSettings:get("veryDebugOn")

local function msg(...)
    if debugOn then
        print(string.format("[%s]: %s", MOD_ID, ...))
    end
end

local function veryVerboseMsg(str, scr)
    if veryDebugOn then
        local s = ""
        if scr ~= nil then
            s = string.format("[%s]", scr)
        end
        print(string.format("%s VeryVerbose %s: %s", MOD_ID, s, str))
    end
end

local function updateDebugs(_, key)
    if key == "debugOn" then
        debugOn = modSettings:get("debugOn")
    elseif key == "veryDebugOn" then
        veryDebugOn = modSettings:get("veryDebugOn")
    end
end
modSettings:subscribe(async:callback(updateDebugs))

local VERSION = 10

return {
    MIN_DELAY = MIN_DELAY,
    MOD_ID = MOD_ID,
    badWeather = badWeather,
    defaultHourAM = 8.5,
    defaultHourPM = 21.5,
    gameHour = gameHour,
    gridToStr = gridToStr,
    isDaytime = isDaytime,
    isNighttime = isNighttime,
    msg = msg,
    oneCell = 8192,
    randDelay = randDelay,
    randOutDelay = randOutDelay,
    veryVerboseMsg = veryVerboseMsg,
    interfaceVersion = VERSION,
    scriptVersion = VERSION
}
