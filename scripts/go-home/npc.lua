local async = require("openmw.async")
local aux_util = require("openmw_aux.util")
local core = require("openmw.core")
local nearby = require("openmw.nearby")
local self = require("openmw.self")
local storage = require("openmw.storage")
local time = require("openmw_aux.time")
local AI = require("openmw.interfaces").AI
local common = require("scripts.go-home.common")
local modSettings = storage.globalSection("SettingsAMod" .. common.MOD_ID)
local delaySettings = storage.globalSection("SettingsBDelay" .. common.MOD_ID)
local timingSettings = storage.globalSection("SettingsCTiming" .. common.MOD_ID)

local FIXME_DURATION_SECS = 15
local MIN_DEFAULT_RANGE = 100
local MIN_DOOR_RANGE = 115

local msg = common.msg
local veryVerboseMsg = common.veryVerboseMsg
local randDelay = common.randDelay

local states = {}
states.GO_TO_DEFAULT = "go-to-default"
states.GO_TO_DOOR = "go-to-door"
states.DEFAULT = "default"
states.HOME = "home"
states.RESET = "reset"
states.UNMANAGED = "unmanaged"

-- Get more random
math.randomseed(os.time())

local badWeather = false
local dead = false
local routine = {state = states.UNMANAGED}

-- Start actual NPC logic
local function changeState(newState)
    msg(string.format("State change %s: %s -> %s", self.recordId, routine.state, newState))
    routine.state = newState
end

local function goToDefault()
    local active = AI.getActivePackage()
    if active ~= nil and active.type ~= "Wander" and active.type ~= "Unknown" then return end --ll
    veryVerboseMsg(string.format("Go to default for actor: %s", self.recordId))
    AI.startPackage({type = "Travel", cancelOther = false, destPosition = routine.defaultPosition})
end

local function goToDoor()
    routine.waitingToWalk = nil --ll
    routine.rand = nil --ll
    local active = AI.getActivePackage()
    if active ~= nil and active.type ~= "Wander" and active.type ~= "Unknown" then --ll
		changeState(states.DEFAULT) --ll
		return --ll
	end --ll
    veryVerboseMsg(string.format("Go to door for actor: %s", self.recordId))
    AI.startPackage({type = "Travel", cancelOther = false, destPosition = routine.doorPosition})
end

local function enterHome()
    veryVerboseMsg(string.format("Enter home for actor: %s", self.recordId))
    AI.removePackages("Travel")
    changeState(states.HOME)
    core.sendGlobalEvent(
        "momw_gh_useDoor",
        {
            actor = self,
            cellName = routine.doorDestCellName,
            targetPos = routine.doorDestPos,
            targetRot = routine.doorDestRot
        }
    )
end

local function noRoutine()
    return routine.state == states.UNMANAGED
        or (dead or routine.init == false or routine.disabledForQuest or routine.fixme)
end

local delayCallbackDoor = async:registerTimerCallback(
    "momw_gh_delayDoor",
    function()
        -- A callback could be left over after a factory reset
        if noRoutine() then return end
        local hour = common.gameHour()
        local hourAM = modSettings:get("hourAM")
        local hourPM = modSettings:get("hourPM")
        local doNightSchedule = common.isNighttime(hour, hourAM, hourPM) or badWeather
        if doNightSchedule then
            veryVerboseMsg(string.format("Execute door callback for actor: %s", self.recordId))
            goToDoor()
        else
            routine.waitingToWalk = nil
            routine.rand = nil
        end
    end
)

local function doGoToDoor(forWeather)
    if routine.waitingToWalk then return end
    routine.rand = math.random(0, delaySettings:get("maxDelay"))
    if forWeather ~= true then
        routine.rand = randDelay()
        if delaySettings:get("rareLongDelay") then
            local longerDelayChance = math.random(1, 50)
            if longerDelayChance == 50 then
                msg(string.format("Rare long delay for actor: %s", self.recordId))
                routine.rand = routine.rand + 300
            end
        end
    end
    veryVerboseMsg(string.format("Door callback delay %s for actor: %s", routine.rand, self.recordId))
    async:newSimulationTimer(routine.rand, delayCallbackDoor)
    routine.waitingToWalk = true
end

local function resetAI()
    AI.removePackages()
    AI.startPackage({
            type = routine.defaultAIType,
            distance = routine.defaultAIDistance,
            duration = routine.defaultAIDuration,
            idle = routine.defaultAIIdle,
            isRepeat = routine.defaultAIIsRepeat
    })
end

local function resetState()
    msg(string.format("Reset state for actor: %s", self.recordId))
    local state = routine.state
    changeState(states.RESET)
    if state == states.DEFAULT then
        changeState(states.DEFAULT)
        resetAI()
    elseif state == states.GO_TO_DOOR then
        changeState(states.GO_TO_DOOR)
        doGoToDoor()
    elseif state == states.HOME then
        changeState(states.HOME)
        resetAI()
    elseif state == states.GO_TO_DEFAULT then
        changeState(states.GO_TO_DEFAULT)
        goToDefault()
    elseif state == "Unknown" then
        changeState(states.DEFAULT)
        resetAI()
    end
end

local function teleportMeBackToDefaultPos()
    --TODO: Wrap teleporting in a player LOS check
    core.sendGlobalEvent(
        "momw_gh_teleportMe",
        {
            actor = self,
            pos = routine.defaultPosition
        }
    )
end

-- This is the NPC state machine
local stateMem = "-1" --ll
local timeTrig --ll
local function update()
    if noRoutine() then return end
    --TODO: Chance no go home
    -- veryVerboseMsg("Updating...")
    if badWeather == true and modSettings:get("goHomeForBadWeather") == false then
        -- Reset bad weather if we're no longer monitoring it
        badWeather = false
    end
    if routine.doorPosition == nil then return end
    if routine.disabledForQuest == true then return end

    local active = AI.getActivePackage()
    if active ~= nil and active.type ~= "Wander" and active.type ~= "Unknown" then --ll
        -- Don't try to handle an NPC if they have an AI different than "Wander" and "Unknown" --ll
        return
    end

    -- Finally, do work
    local amOutside = self.cell.isExterior or self.cell:hasTag("QuasiExterior")
    local hour = common.gameHour()
    local distToDefault = (self.position - routine.defaultPosition):length()
    local reachedDefault = distToDefault <= MIN_DEFAULT_RANGE
    local distToDoor = (self.position - routine.doorPosition):length()
    local hourAM = modSettings:get("hourAM")
    local hourPM = modSettings:get("hourPM")
    local doDaySchedule = common.isDaytime(hour, hourAM, hourPM) and not badWeather
    local doNightSchedule = common.isNighttime(hour, hourAM, hourPM) or badWeather

	-- Reset the NPC "state" to default when he is stuck in the same "state" for more than 6 hours: --ll:
    if routine.state == states.DEFAULT or routine.state == states.GO_TO_DOOR or routine.state == states.GO_TO_DEFAULT then
	    if routine.state ~= stateMem then
		    stateMem = routine.state
		    timeTrig = core.getGameTime() + 21600 -- +21600 = +6h game time
	    elseif core.getGameTime() > timeTrig then
		    changeState(states.DEFAULT)
		    stateMem = "-1"
		    routine.waitingToWalk = nil
		    routine.rand = nil
	    end
    end

    if doDaySchedule then
        if amOutside then
            if routine.state == states.GO_TO_DOOR then
                goToDefault()
                changeState(states.GO_TO_DEFAULT)
            elseif routine.state == states.GO_TO_DEFAULT and reachedDefault then
                AI.removePackages("Travel")
                changeState(states.DEFAULT)
            elseif routine.state == states.HOME then
                goToDefault()
                changeState(states.GO_TO_DEFAULT)
            end
        else -- Indoors
            if routine.state == states.HOME then
                goToDefault()
                changeState(states.GO_TO_DEFAULT)
            end
        end
    elseif doNightSchedule then
        if amOutside then
            if routine.state == states.DEFAULT or routine.state == states.GO_TO_DEFAULT then
                doGoToDoor()
                changeState(states.GO_TO_DOOR)
            elseif routine.state == states.GO_TO_DOOR and distToDoor <= MIN_DOOR_RANGE then
                enterHome()
            elseif routine.state == states.GO_TO_DOOR and routine.rand == nil and AI.getActivePackage().type == routine.defaultAIType then
                msg(string.format("NPC in go-to-door with nil rand and default AI: %s", self.recordId))
                msg(string.format("TODO: Something that shouldn't happen has happened!", self.recordId))
                resetState()
            elseif routine.state == states.HOME then
                -- This shouldn't happen but seems to anyways. This particular
                -- branch will be left in place same as the above just in case
                -- the unexpected happens so we can recover.
                -- Based on observations while testing, this _seems_ to occur
                -- when there's bad weather during the day.
                msg(string.format("NPC in HOME state but outside: %s", self.recordId))
                doGoToDoor()
                changeState(states.GO_TO_DOOR)
            end
        else -- Indoors
            if routine.state == states.HOME then
                -- Remove Travel in case it didn't end on its own
                AI.removePackages("Travel")
            end
        end
    else
        -- No really, this shouldn't _ever_ happen!
        msg(string.format("WARNING: This should never happen! %s", self.recordId))
    end
end

local function runUpdate()
    return time.runRepeatedly(update, timingSettings:get("updateFreq") * time.second)
end
local stopUpdate = runUpdate()

-- This gets ran when the updateFreq value changes e.g., via the script settings menu when the user changes it
local function updateUpdater()
    veryVerboseMsg("Restarting updater....")
    stopUpdate()
    stopUpdate = runUpdate()
end

-- Event handlers
local function Died()
    dead = true
    core.sendGlobalEvent("momw_gh_actorDied", self)
end

local function factoryReset()
    msg(string.format("Factory reset on actor: %s", self.recordId))
    if routine.defaultAIType ~= nil then
        resetAI()
    end
    routine = {
        state = states.UNMANAGED,
        dead = dead,
        init = false
    }
end

-- This happens the first time this mod "scans" an NPC (or after a factory reset)
local tryCount = 0
local tryCountMax = 5
local function findHome(noFindDoor)
    msg(string.format("Find home for actor: %s", self.recordId))
    local active = AI.getActivePackage()
    local invalidAI = active == nil or (active ~= nil and active.type ~= "Wander")
    if invalidAI and tryCount < tryCountMax then --TODO: also maybe if active ~= nil and type == Unknown?!
        -- This NPC is not yet fully loaded, their AI is not yet started
        tryCount = tryCount + 1
        msg(string.format("Actor tries again (%s): %s", tryCount, self.recordId))
        core.sendGlobalEvent("momw_gh_tryAgain", self)
        return
    elseif active == nil and tryCount == tryCountMax then
        msg(string.format("Actor reached try count max (%s); likely has no default AI: %s", tryCountMax, self.recordId))
    end

    routine.state = states.DEFAULT
    routine.defaultCell = self.cell.name
    routine.defaultCellGridX = self.cell.gridX
    routine.defaultCellGridY = self.cell.gridY
    routine.defaultPosition = self.startingPosition
    routine.defaultRotation = self.startingRotation

    -- Gather AI data if we can/need to
    if active ~= nil and active.type == "Wander" then
        routine.defaultAIType = active.type
        routine.defaultAIDistance = active.distance
        routine.defaultAIDuration = active.duration
        routine.defaultAIIdle = active.idle
        routine.defaultAIIsRepeat = active.isRepeat
    else
        -- Other packages are probably too risky to try and mess with
        noFindDoor = true
    end
    routine.disabledForQuest = false
    routine.region = self.cell.region

    local targetDoor
    if noFindDoor ~= true then
        for _, door in pairs(nearby.doors) do
            local destCell = door.type.destCell(door)

            -- Try to get first and last names
            local t = {}
            -- NPCs in TR often have IDs with a prefix like "tr_" which can unfortunately get incorrectly matched
            for s in string.gmatch(self.recordId, "([^ ]+)") do table.insert(t, s) end
            for s in string.gmatch(self.recordId, "([^_]+)") do if s ~= "tr" then table.insert(t, s) end end -- e.g., junal_lei
            if destCell ~= nil
            -- Try to match NPCs to door destCells
                and string.match(string.lower(destCell.name), string.lower(self.recordId))
            -- False match -- Ian and Argonian
                and not (self.recordId == "ian" and string.match(string.lower(destCell.name), "argonian"))
            -- Try to match first and last names individually
                or (t[1] ~= nil and noFindDoor == nil and destCell ~= nil and string.match(string.lower(destCell.name), string.lower(t[1]))
                    or t[2] ~= nil and noFindDoor == nil and destCell ~= nil and string.match(string.lower(destCell.name), string.lower(t[2])))
            then
                --TODO: Need to do a collision test on doorPosition and adjust as needed
                --TODO: in vanilla rararyn radarys can't make it home because his door exit pos collides with the door (I think)
                routine.doorPosition = door.position
                routine.doorDestCellName = door.type.destCell(door).name
                routine.doorDestPos = door.type.destPosition(door)
                routine.doorDestRot = door.type.destRotation(door)
                targetDoor = door
                break
            end
        end
    end

    -- This actor has everything they need to fully Go Home!
    routine.init = true
    core.sendGlobalEvent(
        "momw_gh_setRoutined",
        {actor = self, door = targetDoor, defaults = {
             defaultCell = routine.defaultCell,
             defaultCellGridX = routine.defaultCellGridX,
             defaultCellGridY = routine.defaultCellGridY,
             defaultPos = routine.defaultPosition,
             defaultRot = routine.defaultRotation
        }}
    )
end

local function toggleForQuest(bool)
    msg(string.format("Toggle for quest on actor: %s", self.recordId))
    routine.disabledForQuest = bool
    if not dead then
        AI.removePackages("Travel")
    end
end

local function weatherMonitor(weather)
    -- We don't want to return nil here so make it false if there's no hit
    badWeather = common.badWeather[weather] or false
    -- veryVerboseMsg(string.format("Updating badWeather: %s", badWeather))
end

-- Engine handlers
local function onLoad(data)
    if not data then return end
    badWeather = data.badWeather
    dead = data.dead
    routine = data.routine
end

local function onSave()
    return {
        badWeather = badWeather,
        dead = dead,
        routine = routine,
        scriptVersion = common.scriptVersion
    }
end

local fixMeCallback = async:registerTimerCallback(
    "momw_gh_fixMe",
    function()
        print(string.format("GoHome: Ending FixMe for actor: %s", self.recordId))
        routine.fixme = nil
    end
)

-- Interface
local function fixMe()
    if noRoutine() then
        print(string.format("GoHome: NOT executing FixMe for actor (they are unmanaged or already in FixMe): %s", self.recordId))
        return
    end
    print(string.format("GoHome: Executing FixMe for actor: %s", self.recordId))
    resetAI()
    changeState(states.DEFAULT)
    routine.rand = nil
    routine.fixme = true
    teleportMeBackToDefaultPos()
    async:newSimulationTimer(FIXME_DURATION_SECS, fixMeCallback)
end

local function i()
    print("")
    print("!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!")
    print(string.format("Begin info For NPC: %s", self.recordId))
    print("!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!")
    if routine.state == states.UNMANAGED then
        print("!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!")
        print(string.format("End info For NPC: %s", self.recordId))
        print("!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!")
        return
    end
    print(string.format("badWeather: %s", badWeather))
    print(string.format("dead: %s", dead))
    print(string.format("defaultAIType: %s", routine.defaultAIType))
    print(string.format("defaultAIDistance: %s", routine.defaultAIDistance))
    print(string.format("defaultAIDuration: %s", routine.defaultAIDuration))
    if routine.defaultAIIdle then
        print(string.format("defaultAIIdle.idle2: %s", routine.defaultAIIdle.idle2))
        print(string.format("defaultAIIdle.idle3: %s", routine.defaultAIIdle.idle3))
        print(string.format("defaultAIIdle.idle4: %s", routine.defaultAIIdle.idle4))
        print(string.format("defaultAIIdle.idle5: %s", routine.defaultAIIdle.idle5))
        print(string.format("defaultAIIdle.idle6: %s", routine.defaultAIIdle.idle6))
        print(string.format("defaultAIIdle.idle7: %s", routine.defaultAIIdle.idle7))
        print(string.format("defaultAIIdle.idle8: %s", routine.defaultAIIdle.idle8))
        print(string.format("defaultAIIdle.idle9: %s", routine.defaultAIIdle.idle9))
    end
    print(string.format("defaultAIIsRepeat: %s", routine.defaultAIIsRepeat))
    print(string.format("defaultCell: %s", routine.defaultCell))
    print(string.format("defaultCellGridX: %s", routine.defaultCellGridX))
    print(string.format("defaultCellGridY: %s", routine.defaultCellGridY))
    print(string.format("defaultPosition: %s", routine.defaultPosition))
    print(string.format("defaultRotation: %s", routine.defaultRotation))
    print(string.format("disabledForQuest: %s", routine.disabledForQuest))
    print(string.format("doorDestCellName: %s", routine.doorDestCellName))
    print(string.format("doorDestPos: %s", routine.doorDestPos))
    print(string.format("doorDestRot: %s", routine.doorDestRot))
    print(string.format("doorExitPos: %s", routine.doorExitPos))
    print(string.format("doorPosition: %s", routine.doorPosition))
    print(string.format("fixme: %s", routine.fixme))
    print(string.format("init: %s", routine.init))
    print(string.format("rand: %s", routine.rand))
    print(string.format("region: %s", routine.region))
    print(string.format("waitingToWalk: %s", routine.waitingToWalk))
    local curAI = AI.getActivePackage()
    local curAIstr = "current AI pkg: %s"
    if curAI == nil then
        print(string.format(curAIstr, curAI))
    else
        print(string.format(curAIstr, curAI.type))
    end
    print(string.format("state: %s", routine.state))
    core.sendGlobalEvent("momw_gh_showNPC", self.id)
end

local function infoDump()
    print(aux_util.deepToString(routine, 4))
    print(string.format("END GoHome data dump for NPC: %s (id: %s)", self.recordId, self.id))
end

-- Handoff to the engine
return {
    engineHandlers = {
        onLoad = onLoad,
        onSave = onSave
    },
    eventHandlers = {
        Died = Died,
        momw_gh_factoryResetNPC = factoryReset,
        momw_gh_findHome = findHome,
        momw_gh_infoDump = infoDump,
        momw_gh_npc_updateUpdater = updateUpdater,
        momw_gh_toggleForQuest = toggleForQuest,
        momw_gh_weatherMonitor = weatherMonitor
    },
    interfaceName = common.MOD_ID,
    interface = {
        version = common.interfaceVersion,
        i = i,
        FixMe = fixMe
    }
}
