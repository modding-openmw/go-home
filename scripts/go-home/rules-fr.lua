local GoHome = require("openmw.interfaces").GoHome

if not GoHome then
    error("ERROR: Go Home! is not installed!")
end

print("[GoHome]: Enabling French language support for named cells!")
GoHome.RegisterNamedCellWhitelist({
        "Camp des Ahemmusas",
        "Ald Rédaynia", -- Technically from modded, or populated by mods
        "Ald Vélothi",
        "Ald'ruhn",
        "Plantation Arvel",
        -- "Bal Isra", --TODO
        "Balmora",
        "Caldéra",
        "Dagon fel",
        "Plantation Dren",
        "Coeurébène",
        "Camp des Erabenimsuns",
        "Gnaar mok",
        "Gnisis",
        "Hla oad",
        "Khuul",
        "Maar gan",
        "Molag mar",
        "Longsanglot, Villedieu",
        "Longsanglot, grand bazar",
        "Longsanglot, place Brindisi Dorom",
        "Longsanglot, palais royal : cour",
        "Longsanglot, cour du temple",
        "Pélagiad",
        "Sadrith Mora",
        "Seyda Nihyn",
        "Village Skaal",
        "Suran",
        "Tel Aruhn",
        "Tel branora",
        "Tel Mora",
        "Tel vos",
        "Camp des Urshilakus",
        "Vivec, l'Arène",
        "Vivec, quartier étranger",
        "Vivec, quartier Hlaalu",
        "Vivec, quartier Rédoran",
        "Vivec, canton de Saint-Délyn",
        "Vivec, canton de Saint-Olms",
        "Vivec, quartier Telvanni",
        "Vivec, quartier du Temple",
        "Vos",
        "Complexe du Serval",
        "Camp des Zaïnabs"
})
