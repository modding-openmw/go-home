return {
    gridCellWhitelist = {
        {x = -21, y = 24},
        {x = -7, y = 6},
        {x = -5, y = 4},
        {x = -1, y = -2}, -- The eastern edge of Caldera
        {x = -1, y = -7},
        {x = -1, y = 6}, -- The western edge of Ald-ruhn
        {x = 1, y = -9},
        {x = 3, y = -6},
        {x = 4, y = -5},
        {x = 4, y = -7},
        {x = 4, y = -8},
        {x = 5, y = -6},
        {x = 5, y = -8}
    },
    namedCellWhitelistEN = {
        "Ahemmusa Camp",
        "Ald Velothi",
        "Ald-ruhn",
        "Arvel Plantation",
        -- "Bal Isra", --TODO
        "Balmora",
        "Caldera",
        "Dagon Fel",
        "Dren Plantation",
        "Ebonheart",
        "Erabenimsun Camp",
        "Gnaar Mok",
        "Gnisis",
        "Hla Oad",
        "Khuul",
        "Maar Gan",
        "Molag Mar",
        "Mournhold, Godsreach",
        "Mournhold, Great Bazaar",
        "Mournhold, Plaza Brindisi Dorom",
        "Mournhold, Royal Palace: Courtyard",
        "Mournhold, Temple Courtyard",
        "Pelagiad",
        "Sadrith Mora",
        "Seyda Neen",
        "Skaal Village",
        "Suran",
        "Tel Aruhn",
        "Tel Branora",
        "Tel Mora",
        "Tel Vos",
        "Urshilaku Camp",
        "Vivec, Arena",
        "Vivec, Foreign Quarter",
        "Vivec, Hlaalu",
        "Vivec, Redoran",
        "Vivec, St. Delyn",
        "Vivec, St. Olms",
        "Vivec, Telvanni",
        "Vivec, Temple",
        "Vos",
        "Wolverine Hall",
        "Zainab Camp"
    },

    -- CRF
    crfhGridCellWhitelist = {
        {x = -7, y = 10},
        {x = -9, y = 8}
    },
    crfrNamedCellWhitelist = {
        "Ald Redaynia"
    },

    -- PC
    pcGridCellWhitelist = {
        {x = -115, y = -48},  -- Brina Cross Granary
        {x = -116, y = -48},  -- Felcius Farm
        {x = -125, y = -46},  -- Karam's Shack
        {x = -117, y = -47},  -- Weylus Orchard
    },
    pcNamedCellWhitelist = {
        "Anvil, Garden Quarter",
        "Anvil, Little Arenthia",
        "Anvil, Marina",
        "Anvil, Port Quarter",
        "Archad",
        "Brina Cross",
        "Charach",
        "Hal Sadek",
        "Marav",
        "Salthearth",
        "Talgiana Orchard",
        "Thresvy"
    },

    -- SHOTN
    shotnNamedCellWhitelist = {
        "Bailcnoss",
        "Criaglorc",
        "Dragonstar East",
        "Dragonstar West",
        "Haimtir",
        "Karthgad",
        "Karthwasten",
        "Mairager",
        "Merduibh",
        "Nargozh Camp",
        "Uramok Camp"
    },

    -- TR
    trGridCellWhitelist = {
        {x = 2, y = -20},  -- Mundrethi Plantation
        {x = 36, y = -19}, -- Evos
        {x = 37, y = -19}  -- Evos
    },
    -- Should cover most of the tier I-VI locations from this link:
    -- https://en.uesp.net/wiki/Tamriel_Rebuilt:Cities_%26_Towns
    trNamedCellWhitelist = {
        "Aimrah",
        "Akamora",
        "Almas Thirr",
        "Alt Bosara", -- No supported NPCs here!
        "Ammar",      -- No supported NPCs here!
        "Andar Mok",
        "Andothren",
        "Andothren, Docks",
        "Arvud",
        "Ashamul", -- No supported NPCs here!
        "Bahrammu",
        "Bal Oyra",
        "Baldrahn",
        "Bodrum",
        "Bodrum, Quarry",
        "Bosmora",
        "Darvonis",
        "Dondril",
        "Dreynim", -- Good testing spot?
        "Ebon Tower",
        "Enamor Dayn",
        "Erethan Plantation", -- No supported NPCs here!
        -- "Evos",            -- Not a named cell!
        "Felms Ithul",
        "Firewatch",
        "Gah Sadrith",
        "Gol Mok",
        "Gorne",
        "Helnim",
        "Hla Bulor",
        "Indal-ruhn",
        "Ishanuran Camp",
        "Llothanis",
        "Marog", -- No supported NPCs here!
        "Menaan",
        "Meralag",
        -- "Mundrethi Plantation", -- Not a named cell!
        "Necrom",
        "Necrom, Temple Courtyard",
        "Necrom, Upper District",
        "Necrom, Waterfront",
        "Nivalis",
        "Obainat Camp",
        "Old Ebonheart",
        "Old Ebonheart, Docks",
        "Omaynis",
        "Oran Plantation",
        "Port Telvannis",
        "Ranyon-ruhn",
        "Rilsoan",
        "Roa Dyr",
        "Sadas Plantation",
        "Sailen",
        "Seitur",
        "Selyn",
        "Savrethi Distillery",
        "Tahvel",
        "Tel Aranyon",
        "Tel Gilan",
        "Tel Mothrivra",
        "Tel Muthada",
        "Tel Ouada",
        "Tel Rivus",
        "Teyn",
        "Uman", -- No supported NPCs here!
        "Vathras Plantation",
        "Vhul"
    }
}
