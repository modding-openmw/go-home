local async = require("openmw.async")
local aux_util = require("openmw_aux.util")
local core = require("openmw.core")
local storage = require("openmw.storage")
local time = require("openmw_aux.time")
local types = require("openmw.types")
local world = require("openmw.world")
local I = require("openmw.interfaces")
local common = require("scripts.go-home.common")
local commonData = require("scripts.go-home.commonData")
local modSettings = storage.globalSection("SettingsAMod" .. common.MOD_ID)
local delaySettings = storage.globalSection("SettingsBDelay" .. common.MOD_ID)
local timingSettings = storage.globalSection("SettingsCTiming" .. common.MOD_ID)
local factoryResetSettings = storage.globalSection("SettingsZFactoryReset" .. common.MOD_ID)

if not core.contentFiles.has("go-home.omwaddon") then
    error(core.l10n(common.MOD_ID)("needOmwAddon"))
end

local msg = common.msg
local veryVerboseMsg = common.veryVerboseMsg
local randDelay = common.randDelay

local actorBlacklist = {}
local cellWhitelist = {}
local cellGridWhitelist = {}
local modBlacklist = {}

local disabled = factoryResetSettings:get("factoryReset")

local actorBlacklistInternal = {}
local actorQuests = {}
local routined = {}
local goneHome = {}
local merchants = {}
local noFindHome = {}
local questExceptions = {}
local scriptWhitelist = {}

I.Settings.registerGroup {
    key = "SettingsAMod" .. common.MOD_ID,
    page = common.MOD_ID,
    l10n = common.MOD_ID,
    name = "modSettingsTitle",
    description = "modSettingsDesc",
    permanentStorage = false,
    settings = {
        {
            key = "disableOnly",
            name = "disableOnly_name",
            description = "disableOnly_desc",
            default = false,
            renderer = "checkbox"
        },
        {
            key = "minDisableDist",
            name = "minDisableDist_name",
            description = "minDisableDist_desc",
            default = 682,
            min = 0,
            max = 1364,
            renderer = "number"
        },
        {
            key = "hourAM",
            name = "hourAM_name",
            description = "hourAM_desc",
            default = common.defaultHourAM,
            min = 1,
            max = 12,
            renderer = "number"
        },
        {
            key = "hourPM",
            name = "hourPM_name",
            description = "hourPM_desc",
            default = common.defaultHourPM,
            min = 12,
            max = 23,
            renderer = "number"
        },
        {
            key = "goHomeForBadWeather",
            name = "goHomeForBadWeather_name",
            description = "goHomeForBadWeather_desc",
            default = true,
            renderer = "checkbox"
        },
        {
            key = "chanceNoDisable",
            name = "chanceNoDisable_name",
            description = "chanceNoDisable_desc",
            default = true,
            renderer = "checkbox"
        },
        {
            key = "chanceNoDisableNum",
            name = "chanceNoDisableNum_name",
            description = "chanceNoDisableNum_desc",
            default = 10, -- Smaller is more!
            min = 1,
            max = 100,
            renderer = "number"
        },
        {
            key = "merchantChanceNoDisableNum",
            name = "merchantChanceNoDisableNum_name",
            description = "merchantChanceNoDisableNum_desc",
            default = 5, -- Smaller is more!
            min = 1,
            max = 100,
            renderer = "number"
        },
        {
            key = "chanceNoDisableAddWeather",
            name = "chanceNoDisableAddWeather_name",
            description = "chanceNoDisableAddWeather_desc",
            default = 5, -- Smaller is more!
            min = 0,
            max = 100,
            renderer = "number"
        },
        {
            key = "useRenderingRaycast",
            name = "useRenderingRaycast_name",
            description = "useRenderingRaycast_desc",
            default = false,
            renderer = "checkbox"
        },
        {
            key = "debugOn",
            name = "debugOn_name",
            description = "debugOn_desc",
            default = false,
            renderer = "checkbox"
        },
        {
            key = "veryDebugOn",
            name = "veryDebugOn_name",
            description = "veryDebugOn_desc",
            default = false,
            renderer = "checkbox"
        }
    }
}

I.Settings.registerGroup {
    key = "SettingsBDelay" .. common.MOD_ID,
    page = common.MOD_ID,
    l10n = common.MOD_ID,
    name = "delaySettingsTitle",
    description = "delaySettingsDesc",
    permanentStorage = false,
    settings = {
        {
            key = "maxDelay",
            name = "maxDelay_name",
            description = "maxDelay_desc",
            default = 30,
            min = 1,
            max = 120,
            renderer = "number"
        },
        {
            key = "maxOutDelay",
            name = "maxOutDelay_name",
            description = "maxOutDelay_desc",
            default = 10,
            min = 0,
            max = 120,
            renderer = "number"
        },
        {
            key = "initialWeatherDelay",
            name = "initialWeatherDelay_name",
            description = "initialWeatherDelay_desc",
            default = 60,
            min = 1,
            max = 300,
            renderer = "number"
        },
        {
            key = "rareLongDelay",
            name = "rareLongDelay_name",
            description = "rareLongDelay_desc",
            default = true,
            renderer = "checkbox"
        }
    }
}

I.Settings.registerGroup {
    key = "SettingsCTiming" .. common.MOD_ID,
    page = common.MOD_ID,
    l10n = common.MOD_ID,
    name = "timingSettingsTitle",
    description = "timingSettingsDesc",
    permanentStorage = false,
    settings = {
        {
            key = "weatherMonitorFreq",
            name = "weatherMonitorFreq_name",
            description = "weatherMonitorFreq_desc",
            default = 30,
            min = 1,
            max = 300,
            renderer = "number"
        },
        {
            key = "updateFreq",
            name = "updateFreq_name",
            description = "updateFreq_desc",
            default = 0.5,
            min = 0.1,
            max = 10,
            renderer = "number"
        }
    }
}

I.Settings.registerGroup {
    key = "SettingsZFactoryReset" .. common.MOD_ID,
    page = common.MOD_ID,
    l10n = common.MOD_ID,
    name = "factoryResetSettingsTitle",
    description = "factoryResetSettingsDesc",
    permanentStorage = false,
    settings = {
        {
            key = "factoryReset",
            name = "factoryReset_name",
            description = "factoryReset_desc",
            default = false,
            renderer = "checkbox"
        }
    }
}

local hourAM = modSettings:get("hourAM")
local hourPM = modSettings:get("hourPM")
local function updateHours(_, key)
    if key == "hourAM" then
        hourAM = modSettings:get("hourAM")
    elseif key == "hourPM" then
        hourPM = modSettings:get("hourPM")
    end
end
modSettings:subscribe(async:callback(updateHours))

-- Filter NPCs to decide if we should do anything with them
local function onActorActive(actor)
    if disabled == true then return end
    local actorId = actor.id
    local actorRecordId = actor.recordId

    -- Bail out if this actor has already been blacklisted
    if actorBlacklistInternal[actorRecordId] then return end

    -- This NPC has been processed and is managed by us
    if routined[actorId] ~= nil and routined[actorId].init then return end

    -- Ignore non-NPCs
    if actor.type ~= types.NPC then
        veryVerboseMsg(string.format("Actor denied because they aren't an NPC: %s", actorRecordId))
        actorBlacklistInternal[actorRecordId] = true
        return
    end

    -- Don't process explicitly blacklisted actors
    if actorBlacklist[actorRecordId] then
        msg(string.format("Actor denied via actor blacklist: %s", actorRecordId))
        actorBlacklistInternal[actorRecordId] = true
        return
    end

    -- Internal filter
    if actorBlacklistInternal[actorRecordId] then return end

    if modBlacklist[actor.contentFile] then
        msg(string.format("Actor denied via mod blacklist: %s (%s)", actorRecordId, actor.contentFile))
        actorBlacklistInternal[actorRecordId] = true
        return
    end

    -- Don't process dead people
    if actor.type.stats.dynamic.health(actor).base <= 0 then
        msg(string.format("Actor is dead: %s", actorRecordId))
        actorBlacklistInternal[actorRecordId] = true
        return
    end

    -- Ignore dreamers, guards, and ordinators.
    if string.match(actorRecordId, "guard")
        or string.match(actorRecordId, "dreamer_")
        or string.match(actorRecordId, "firemoth_")
        or string.match(actorRecordId, "ordinator")
        or string.match(actorRecordId, " slave male")
        or string.match(actorRecordId, "slave female")
        -- GrumblingVomit uses these e.g., in Repopulated Morrowind/Waters
        or string.match(actorRecordId, "gvrm_")
        or string.match(actorRecordId, "gv_")
        or string.match(actorRecordId, "mwc_slave")
        or string.match(actorRecordId, "market_slave")
        -- DetailDevil NPCs
        or string.match(actorRecordId, "detd_")
        or (string.match(actorRecordId, "roht_")
            -- Do allow some RoHT NPCs to be managed
            and not actorRecordId == "roht_fargorn")
        or string.match(actorRecordId, "telvanni archer")
        or string.match(actorRecordId, "buoyant armiger")
        or string.match(actorRecordId, "imperial archer")
        or string.match(actorRecordId, "imperial templar")
        or string.match(actorRecordId, "ab_telvanniwarwizard")
        or string.match(actorRecordId, "telvanni sharpshooter")
        or string.match(actorRecordId, "tr_m3_imp_archer")
        or string.match(actorRecordId, "tr_m4_red_watch")
        or string.match(actorRecordId, "redoranwatchman")
        or string.match(actorRecordId, "_io_armiger")      -- IO Imperials
        or string.match(actorRecordId, "io_hlaalu archer") -- IO Kinsmer
        or string.match(actorRecordId, "mhbeggar")         -- Immersive Mournhold
    then
        msg(string.format("Actor denied via pattern match blacklist: %s", actorRecordId))
        actorBlacklistInternal[actorRecordId] = true
        return
    end

    -- Named or grid cell whitelist
    local actorCell = actor.cell
    if actorCell.name == nil or actorCell.name == "" then
        local s = common.gridToStr(actorCell.gridX, actorCell.gridY)
        if not cellGridWhitelist[s] then
            msg(string.format("Actor denied via cell grid whitelist: %s", actorRecordId))
            actorBlacklistInternal[actorRecordId] = true
            return
        end
    elseif not cellWhitelist[actorCell.name] then
        msg(string.format("Actor denied via named cell whitelist: %s", actorRecordId))
        actorBlacklistInternal[actorRecordId] = true
        return
    end

    -- Ignore NPCs with MWScript attached to them
    local mws = actor.type.record(actor).mwscript
    if mws and not scriptWhitelist[mws] then
        msg(string.format("Actor \"%s\" MWScript denied: %s", actorRecordId, mws))
        actorBlacklistInternal[actorRecordId] = true
        return
    end

    -- Ignore Travel NPCs
    for service, offered in pairs(actor.type.record(actor).servicesOffered) do
        if service == "Travel" and offered == true then
            msg(string.format("Actor denied via service check: %s", actorRecordId))
            actorBlacklistInternal[actorRecordId] = true
            return
        elseif service ~= "Travel" and offered == true then
            -- Note other merchants for later
            merchants[actorId] = true
        end
    end

    -- Build quest exception data
    for actorRecId, questData in pairs(questExceptions) do
        if actorRecordId == actorRecId then
            msg(string.format("Processing quest data for: %s", actorRecordId))
            actorQuests[actor.id] = questData
        end
    end

    -- Okay, get routine'd
    actor:sendEvent("momw_gh_findHome", modSettings:get("disableOnly") or noFindHome[actorRecordId])
end

local function getCurrentWeather()
    return world.mwscript.getGlobalScript("momw_gh_weather_monitor").variables.cur
end

local function runWeatherMonitor()
    return time.runRepeatedly(
        function()
            -- veryVerboseMsg("[Global] Running weather monitor")
            world.players[1]:sendEvent("momw_gh_weatherMonitor", getCurrentWeather())
        end,
        timingSettings:get("weatherMonitorFreq") * time.second,
        {initialDelay = delaySettings:get("initialWeatherDelay") * time.second}
    )
end
local stopWeatherMonitor = runWeatherMonitor()
if modSettings:get("goHomeForBadWeather") == false then
    msg("Weather monitor is disabled")
    stopWeatherMonitor()
end

local function getLocalActors()
    world.players[1]:sendEvent("momw_gh_getLocalActors")
end

local function disableMe(d)
    world.players[1]:sendEvent("momw_gh_checkLOS", d.actor)
end

-- This receives a list of nearby actors from the player
local function tryToGoHome(npcs)
    -- Don't do any of this if it's not day time or bad weather; get a new
    -- value since some time may have passed since the delayed time was
    -- scheduled. Also settings may have changed, so get them fresh.
    local hour = common.gameHour()
    local weather = getCurrentWeather()
    local badWeather = modSettings:get("goHomeForBadWeather") == true
        and (common.badWeather[weather] or false)
    if common.isDaytime(hour, hourAM, hourPM) and not badWeather then return end

    for _, npc in pairs(npcs) do
        local id = npc.id
        local data = routined[id]
        if data ~= nil then
            if data.dead == true or data.enabled == false or data.fakeDisabled == true then
                goto nextHomeAttempt
            end
            if data.noDoor == true and data.actor ~= nil and routined[id].enabled and not routined[id].scheduledDisable then
                if modSettings:get("chanceNoDisable") then
                    local chance = modSettings:get("chanceNoDisableNum")
                    if merchants[id] then
                        chance = modSettings:get("merchantChanceNoDisableNum")
                    end
                    if badWeather == true then
                        chance = chance + modSettings:get("chanceNoDisableAddWeather")
                    end
                    veryVerboseMsg(string.format("1 in %s chance no disable for actor: %s", chance, data.actor.recordId))
                    local rand = math.random(1, chance)
                    if rand == chance then
                        msg(string.format("Not disabling actor: %s !!", data.actor.recordId))
                        routined[id].fakeDisabled = true
                        goto nextHomeAttempt
                    end
                end
                routined[id].scheduledDisable = true
            end
            if routined[id].scheduledDisable then
                disableMe({actor = data.actor})
            end
            ::nextHomeAttempt::
        end
    end
end
local tryToGoHomeCallback = time.registerTimerCallback(
    "momw_gh_getLocalActors",
    function() getLocalActors() end
)

local function update()
    if disabled == true then return end
    -- veryVerboseMsg("[Global] Updating...")
    local player = world.players[1]
    for actorId, questData in pairs(actorQuests) do
        if routined[actorId] == nil then goto nextQuestCheck end
        local quest = player.type.quests(player)[questData.quest]

        -- Try to apply quest exceptions
        if quest
            and (quest.stage >= questData.before and quest.stage < questData.after)
            and routined[actorId].disabledForQuest == false and not quest.finished
        then
            msg(string.format("Actor is disabled for quest: %s (%s)", routined[actorId].actor.recordId, questData.quest))
            routined[actorId].disabledForQuest = true
            routined[actorId].actor:sendEvent("momw_gh_toggleForQuest", true)
            -- Reset their position
            routined[actorId].actor:teleport(
                routined[actorId].defaults.defaultCell,
                routined[actorId].defaults.defaultPos,
                routined[actorId].defaults.defaultRot)

        elseif quest
            and (quest.stage < questData.before or quest.stage >= questData.after)
            and routined[actorId].disabledForQuest == true
        then
            msg(string.format("Actor is enabled for quest: %s (%s)", routined[actorId].actor.recordId, questData.quest))
            routined[actorId].disabledForQuest = false
            routined[actorId].actor:sendEvent("momw_gh_toggleForQuest", false)
        end
        ::nextQuestCheck::
    end

    local hour = common.gameHour()
    local weather = getCurrentWeather()
    local badWeather = modSettings:get("goHomeForBadWeather") == true
        and (common.badWeather[weather] or false)
    local doDaySchedule = common.isDaytime(hour, hourAM, hourPM) and not badWeather
    local doNightSchedule = common.isNighttime(hour, hourAM, hourPM) or badWeather

    if doDaySchedule then
        -- Request local NPCs from the player so we don't have to
        -- iterate through every NPC in the database every update
        for _, npc in pairs(goneHome) do
            local id = npc.id
            local data = routined[id]

            -- Ignore unmamaged NPCs
            if data == nil then goto nextRoutined end

            if not data.actor:isValid() then
                -- A mod might have been removed, but whatever the reason this
                -- actor is no longer valid, so clear out its data.
                routined[id] = nil
                goto nextRoutined
            end
            if data.dead == true then goto nextRoutined end
            if data.fakeDisabled == true then
                msg(string.format("Ending fake disable for actor: %s", data.actor.recordId))
                data.fakeDisabled = false
            end
            if data.indoors then
                world.players[1]:sendEvent(
                    "momw_gh_checkToComeOutside",
                    {doorExitPos = data.doorExitPos, npc = data.actor}
                                          )
            end

            local now = core.getGameTime()
            local doCheck = routined[id].enableCheckTime == nil or routined[id].enableCheckTime - now >= time.day
            if data.noDoor == true and data.actor ~= nil and routined[id].enabled == false and doCheck then
                -- Only enable local NPCs, send to player for confirmation
                veryVerboseMsg(string.format("Requesting enable for %s", data.actor.recordId))
                routined[id].enableCheckTime = now
                world.players[1]:sendEvent("momw_gh_checkToEnable", data.actor)
            end
            ::nextRoutined::
        end

    elseif doNightSchedule then
        -- Timer callback tryToGoHomeCallback() that executes tryToGoHome()
        time.newSimulationTimer(randDelay(), tryToGoHomeCallback)
    end
end
local function runUpdate()
    return time.runRepeatedly(update, timingSettings:get("updateFreq") * time.second)
end
local stopUpdate = runUpdate()

local comeOutsideCallback = time.registerTimerCallback(
    "comeOutsideCallback",
    function(d)
        local id = d.id
        if not routined[id].indoors then return end
        routined[id].actor:teleport(
            routined[id].defaults.defaultCell,
            routined[id].doorExitPos,
            routined[id].doorExitRot
        )
        routined[id].indoors = false
    end
)

local function comeOutside(id)
    time.newSimulationTimer(common.randOutDelay(), comeOutsideCallback, {id = id})
end

local enableMeCallback = time.registerTimerCallback(
    "enableMeCallback",
    function(npc)
        local id = npc.id
        routined[id].enabled = true
        routined[id].enableCheckTime = nil
        routined[id].isLocal = nil
        routined[id].scheduledDisable = false
        goneHome[id] = nil
        npc.enabled = true
    end
)
local function enableMe(npc)
    msg(string.format("Enabling local actor: %s", npc.recordId))
    time.newSimulationTimer(common.randOutDelay(), enableMeCallback, npc)
end

-- This NPC was in the player's view when we tried to enable it so
-- allow for another try. This will loop until they can be enabled.
local function retryEnableMe(id)
    veryVerboseMsg(string.format("Retrying enable for ID %s", id))
    if routined[id] then routined[id].enableCheckTime = nil end
end

local function setGoneHome(npc)
    goneHome[npc.id] = npc
end

local function useDoor(data)
    if routined[data.actor.id].indoors then return end
    veryVerboseMsg(string.format("Actor is using a door: %s", data.actor.recordId))
    data.actor:teleport(
        data.cellName,
        data.targetPos,
        data.targetRot
    )
    routined[data.actor.id].indoors = true
    setGoneHome(data.actor)
end

-- Exception interface funcs
local function registerActorBlacklist(actorRecordIds)
    for _, id in pairs(actorRecordIds) do
        msg(string.format("Blacklisting actor: %s", id))
        actorBlacklist[string.lower(id)] = true
    end
end

local function registerGridCellWhitelist(grids)
    for _, g in pairs(grids) do
        msg(string.format("Whitelisting cell: %s, %s", g.x, g.y))
        cellGridWhitelist[common.gridToStr(g.x, g.y)] = true
    end
end

local function registerNamedCellWhitelist(cellNames)
    for _, name in pairs(cellNames) do
        msg(string.format("Whitelisting cell: %s", name))
        cellWhitelist[name] = true
    end
end

local function registerModBlacklist(plugins)
    for _, plugin in pairs(plugins) do
        msg(string.format("Blacklisting mod: %s", plugin))
        modBlacklist[string.lower(plugin)] = true
    end
end

local function registerNoFindHome(actorRecordIds)
    for _, recordId in pairs(actorRecordIds) do
        msg(string.format("Setting noFindHome for actor: %s", recordId))
        noFindHome[string.lower(recordId)] = true
    end
end

local function registerQuestExceptions(data)
    for recordId, questData in pairs(data) do
        msg(string.format("Setting quest exceptions for actor: %s", recordId))
        questExceptions[string.lower(recordId)] = {
            quest = questData.quest,
            before = questData.before,
            after = questData.after
        }
    end
end

local function registerScriptWhitelist(scripts)
    for _, script in pairs(scripts) do
        msg(string.format("Whitelisting script: %s", script))
        scriptWhitelist[string.lower(script)] = true
    end
end

-- Helper interface funcs
local function getNPC(recordId)
    for id, data in pairs(routined) do
        if data.actor:isValid() and data.actor.recordId == string.lower(recordId) then
            print(string.format("START GoHome data dump for NPC: %s (id: %s)", recordId, id))
            print(aux_util.deepToString(data, 4))
            data.actor:sendEvent("momw_gh_infoDump")
            if questExceptions[string.lower(recordId)] then
                print("Quest data:")
                print(aux_util.deepToString(questExceptions[string.lower(recordId)], 4))
            end
        end
    end
end

local function showAll()
    print(aux_util.deepToString(routined, 4))
end

local function showLocalNPCs(data)
    if data.cellName == "" then
        print(string.format("[GoHome]: Showing local NPCs for: %sx%s", data.cellGridX, data.cellGridY))
    else
        print(string.format("[GoHome]: Showing local NPCs for: %s", data.cellName))
    end
    for _, d in pairs(routined) do
        if data.cellName == "" then
            if data.cellGridX == d.defaults.defaultCellGridX and data.cellGridY == d.defaults.defaultCellGridY then
                print(aux_util.deepToString(d, 4))
            end
        else
            if d.defaults.defaultCell == data.cellName then
                print(aux_util.deepToString(d, 4))
            end
        end
    end
end

local function showNPC(id)
    if disabled then
        print("Go Home! is currently disabled and factory reset!")
        return
    end
    local d = routined[id]
    if d == nil then return end
    print("======== Begin global data:")
    print(string.format("enableCheckTime: %s", d.enableCheckTime))
    print(string.format("enabled (engine): %s", d.actor.enabled))
    print(string.format("enabled (mod): %s", d.enabled))
    print(string.format("indoors: %s", d.indoors))
    print(string.format("init: %s", d.init))
    print(string.format("isLocal: %s", d.isLocal))
    print(string.format("noDoor: %s", d.noDoor))
    print(string.format("disabledForQuest: %s", d.disabledForQuest))
    print(string.format("doorExitPos: %s", d.doorExitPos))
    print(string.format("scheduledDisable: %s", d.scheduledDisable))
    if actorQuests[id] then
        print("actorQuests:")
        print(aux_util.deepToString(actorQuests[id]))
    end
    print("!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!")
    print(string.format("End info For NPC: %s", d.actor.recordId))
    print("!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!")
end

-- Exceptions wrapper
local function registerAllDefaults()
    if core.contentFiles.has("Morrowind.esm") then
        print("[GoHome]: Registering support for Morrowind.esm")

        -- Known actors to ignore
        registerActorBlacklist({
                "agronian guy",
                "ano vando",
                "forstaag the sweltering",
                "sur surishpi", -- Ashlander in Vos
                "yan assinabi"  -- Ashlander in Vos
        })

        -- Known good cells with no name to do this in
        registerGridCellWhitelist(commonData.gridCellWhitelist)

        -- Don't load rules for English when we want French
        if not core.contentFiles.has("go-home-fr.omwscripts") then
            -- Known good cells to do this in
            registerNamedCellWhitelist(commonData.namedCellWhitelistEN)
        end

        -- Ignore NPCs from these mods
        registerModBlacklist({
                "Blademeister_v1.5.esp",
                "F&F_base.esm",
                "F&F_BCoM.ESP",
                "F&F_TR.ESP",
                "MCA.esm",
                "RepopulatedBloodmoon.ESP",
                "RepopulatedMainland.ESP",
                "RepopulatedMorrowind.ESM",
                "RepopulatedMorrowind.ESP",
                "RepopulatedMorrowind_MageRobes.ESP",
                "RepopulatedMorrowind_ImmersiveMournhold.ESP",
                "RepopulatedMorrowind_OAAB_Data.ESP",
                "RepopulatedSkyrim.ESP",
                "RepopulatedWaters.ESP",
                "RepopulatedWaters_TR.ESP",
                "Sload and Slavers.ESP",
                "Sloadic Transport.ESP",
                "Starfires NPC Additions ver-1.11.esp",
                "Starfires NPC Additions ver-1.13.esp",
                "wandering_merchants.esp"
        })

        -- Some NPCs can't actually reach their (usually modded)
        -- homes so don't make them try.
        registerNoFindHome({
                "arangaer",
                "assi serimilk",  -- Wrongfully matches "Maar Gan, Assirari Zama-Rasour's Hut"
                "braynas hlervu", --TODO: Some houses just break pathfinding, need an offset
                "elegal",
                "ian"             -- Matches "Argonian Mission" in Ebonheart :S
        })

        -- Ignore NPCs during specified quest points
        local sleepersAwake = {quest = "A1_SleepersAwake", before = 1, after = 50}
        registerQuestExceptions({
                --TODO: Faction stronghold NPCs
                --TODO: Raven Rock NPCs
                ["aeta wave-breaker"] = {quest = "MV_BanditVictim", before = 10, after = 110},
                ["Alvura Othrenim"] = sleepersAwake,
                ["elvil vidron"] = {quest = "TT_FalseIncarnate", before = 10, after = 60},
                ["apelles matius"] = {quest = "TR_DBAttack", before = 30, after = 50},
                ["assi serimilk"] = sleepersAwake,
                ["Assimusa Samsi"] = {quest = "HR_Stronghold", before = 0, after = 250},
                ["balyn omavel"] = {quest = "DA_Mephala", before = 20, after = 60},
                brallion = {quest = "TG_SS_GreedySlaver", before = 10, after = 30},
                ["braynas hlervu"] = {quest = "TG_SS_Generosity1", before = 1, after = 100},
                ["danso indules"] = {quest = "B8_MeetVivec", before = 1, after = 5},
                ["Daynasa Telandas"] = sleepersAwake,
                ["Dralas Gilu"] = sleepersAwake,
                ["drarayne girith"] = sleepersAwake,
                ["Dravasa Andrethi"] = sleepersAwake,
                ["endris dilmyn"] = sleepersAwake,
                ["engar ice-mane"] = {quest = "BM_Trial", before = 10, after = 100}, --TODO: Need a forced door option for this and maybe others
                ["Eralane Hledas"] = sleepersAwake,
                fargoth = {quest = "MS_Lookout", before = 20, after = 40},
                ["gee-pop varis"] = {quest = "TR09_Journalist", before = 1, after = 70},
                hides_his_foot = {quest = "HH_TwinLamps3", before = 50, after = 70},
                ["Llandras Belaal"] = sleepersAwake,
                ["marena gilnith"] = {quest = "MS_MatchMaker", before = 0, after = 120},
                ["mehra helas"]  = {quest = "MS_CrimsonPlague", before = 0, after = 170},
                nedhelas = {quest = "IC16_Haunting", before = 0, after = 15},
                ["neldris llervu"] = sleepersAwake,
                ["Nelmil Hler"] = sleepersAwake,
                ["Rararyn Radarys"] = sleepersAwake,
                ["Relur Faryon"] = sleepersAwake,
                thrud = {quest = "MS_BarbarianBook", before = 0, after = 110},
                ["vireveri darethran"] = sleepersAwake,
                ["Vivyne Andrano"] = sleepersAwake
        })

        -- It's okay to manage NPCs with these scripts
        registerScriptWhitelist({
                "ahemmusakill",
                "balynScript",
                "BILL_TT_Elvil_Vidron",
                "brallionscript",
                "drenslaveowners",
                "druleneScript",
                "erabenimsunkill",
                "h11_beauchamp",
                "hidesfootscript",
                "highpocketsscript",
                "localstate",
                "nolore",
                "sleeperscript",
                "thrudscript",
                "urshilakukill",
                "velnyawalkscript",
                "voduniusscript",
                "warloverkill",
                "zainabkill"
        })
    end

    if core.contentFiles.has("AFFresh.esp") then
        print("[GoHome]: Registering support for AFFresh.esp")
        --TODO: terinde is a good candidate for a forced default position since his actual
        --TODO: default doesn't make sense by the time this mod can possibly manage him
        registerActorBlacklist({"af_terinde"})
    end

    if core.contentFiles.has("an-nwahs-guide-to-modern-culture-and-mythology.omwaddon") then
        print("[GoHome]: Registering support for an-nwahs-guide-to-modern-culture-and-mythology.omwaddon")
        -- To ensure he actually leaves
        registerActorBlacklist({"momw_greeter"})
    end

    if core.contentFiles.has("Beautiful cities of Morrowind.ESP") then
        print("[GoHome]: Registering support for Beautiful cities of Morrowind.ESP")
        registerActorBlacklist({
                "tc_0",
                "tc_stand_0"
        })
        registerNoFindHome({
                "ancola",            -- She can't find her way home with BCOM! Seems like a pathfinding bug..
                "dralcea arethi",    -- Also can't find her way home with BCOM! Same pathfinding bug..?
                "apelles matius",    -- Wrongfully matches "Ebonheart, Pilus Amatius's House" (BCOM)
                "hul",               -- TODO: Home (from BCOM) is found at the wrong point, above where it actually is
                "manicky",           -- (BCOM) Can't walk up a slope to get home :(
                "MwG_Unila Nerethi", -- BCOM Balmora waterworks merchants
                "MwG_Goler Ralvayn"
        })
    else
        registerNoFindHome({
                -- Tries to walk up the stairs to the flat above because
                -- the door position collides with the door itself :/
                --TODO: Needs an offset
                -- Manages fine with BCOM
                "rararyn radarys"
        })
    end

    if core.contentFiles.has("Cutting Room Floor - Herders.esp") then
        print("[GoHome]: Registering support for Cutting Room Floor - Herders.esp")
        registerGridCellWhitelist(commonData.crfhGridCellWhitelist)
    end

    if core.contentFiles.has("Cutting Room Floor - Ald Redaynia.esp") then
        print("[GoHome]: Registering support for Cutting Room Floor - Ald Redaynia.esp")
        registerNamedCellWhitelist(commonData.crfrNamedCellWhitelist)
    end

    if core.contentFiles.has("Drethos_Ancestral_Tomb.ESP") then
        print("[GoHome]: Registering support for Drethos_Ancestral_Tomb.ESP")
        registerQuestExceptions({
                slf_ho_antjurah_drethos = {quest = "slf_ho_drethosancestraltomb", before = 0, after = 90}
        })
    end

    if core.contentFiles.has("OAAB Brother Junipers Twin Lamps.esp") then
        print("[GoHome]: Registering support for OAAB Brother Junipers Twin Lamps.esp")
        registerQuestExceptions({
                ["mollimo of cloudrest"]  = {quest = "TL_Abebaal", before = 20, after = 100}
        })
    end

    if core.contentFiles.has("TR_Mainland.esm") then
        print("[GoHome]: Registering support for TR_Mainland.esm")

        registerActorBlacklist({
                "TR_m3_Fentus Stedanne", -- Census & Excise guy at OE the gate. Does he ever go home?!
        })

        registerGridCellWhitelist(commonData.trGridCellWhitelist)
        registerNamedCellWhitelist(commonData.trNamedCellWhitelist)

        registerNoFindHome({
                "tr_m3_moren silanus", -- She can't get back to her spot :(
                "tr_m4_alrysa faali",  -- She can't get over her doorstep :(
                "tr_m1_q_simethfollow",
                "tr_m1_simeth",
                "tr_m2_simeth hlarin",
                "tr_m1_hekka_gildaren",
                "tr_m3_fjalma"
        })

        registerScriptWhitelist({
                "t_scnpc_mw_map1",
                "t_scnpc_mw_map1nolore",
                "t_scnpc_mw_map2",
                "t_scnpc_mw_map2nolore",
                "t_scnpc_mw_map3",
                "t_scnpc_mw_map3nolore",
                "t_scnpc_mw_map4",
                "t_scnpc_mw_map4nolore",
                "tr_m3_il_merchant1script",
                "tr_m3_npc_oe_common",
                "tr_m3_npc_oe_poor",
                "tr_m3_npc_oe_rich",
                "tr_m3_oe_eecq1_scr"
        })
    end

    if core.contentFiles.has("Sky_Main.esm") then
        print("[GoHome]: Registering support for Sky_Main.esm")

        -- https://en.uesp.net/wiki/Project_Tamriel:Skyrim/Places
        registerNamedCellWhitelist(commonData.shotnNamedCellWhitelist)

        registerScriptWhitelist({
                "t_scnpc_sky_hold0",
                "t_scnpc_sky_hold0_nolore",
                "t_scnpc_sky_hold0_reachman"
        })
    end

    if core.contentFiles.has("Cyr_Main.esm") then
        print("[GoHome]: Registering support for Cyr_Main.esm")

        registerNamedCellWhitelist(commonData.pcNamedCellWhitelist)

        registerScriptWhitelist({
                "t_scnpc_cyr_anvil",
        })

        registerGridCellWhitelist(commonData.pcGridCellWhitelist)
    end
end

-- Init defaults
registerAllDefaults()

-- Engine handlers
local function onLoad(data)
    if not data then return end
    if data.scriptVersion >= 6 then
        for obj, rdata in pairs(data.routined) do
            routined[obj.id] = rdata
            routined[obj.id].actor = obj
        end
    else
        routined = data.routined
    end
    if data.scriptVersion >= 7 then
        for _, npc in pairs(data.goneHome) do
            goneHome[npc.id] = npc
        end
    end
    actorBlacklistInternal = data.actorBlacklistInternal
    actorQuests = data.actorQuests
    disabled = data.disabled
    merchants = data.merchants
    questExceptions = data.questExceptions
end

local function onSave()
    -- We can't save id as our key since if the user changes their load order
    -- the engine can't magically update it, but it can with the object itself
    -- as the key. So, make a new table structured in that way before we save.
    local safeToSaveRoutined = {}
    for _, data in pairs(routined) do
        safeToSaveRoutined[data.actor] = data
    end
    local safeToSaveGoneHome = {}
    for _, npc in pairs(goneHome) do
        safeToSaveGoneHome[npc] = true
    end
    return {
        actorBlacklistInternal = actorBlacklistInternal,
        actorQuests = actorQuests,
        disabled = disabled,
        goneHome = safeToSaveGoneHome,
        merchants = merchants,
        questExceptions = questExceptions,
        routined = safeToSaveRoutined,
        scriptVersion = common.scriptVersion
    }
end

-- Event handlers
local function actorDied(actor)
    if routined[actor.id] then
        msg(string.format("Managed NPC has died: %s", actor.recordId))
        routined[actor.id].dead = true
    end
end

local function cellChanged()
    for id, data in pairs(routined) do
        if data.enabled == false and data.fakeDisabled ~= true and data.isLocal == false then
            retryEnableMe(id)
        end
    end
end

local function factoryReset(force)
    if disabled == true and (factoryResetSettings:get("factoryReset") == false or force) then
        print("Re-enabling the mod after a factory reset ...")
        registerAllDefaults()
        disabled = false
    elseif disabled == false and (factoryResetSettings:get("factoryReset") == true or force) then
        print("Executing factory reset and disabling the mod ...")
        disabled = true

        for actorId, data in pairs(routined) do
            if data.actor:isValid() then
                local actorRecordId = data.actor.recordId
                msg(string.format("Issuing factory reset for NPC: %s", actorRecordId))
                routined[actorId].actor:sendEvent("momw_gh_factoryResetNPC", disabled)

                msg(string.format("Wiping global data for NPC: %s", actorRecordId))
                local d = data.defaults
                if d then
                    -- This can fail for unknown reasons, so pcall() it
                    -- because we need the factory reset to complete fully.
                    local stat, err = pcall(function()
                            routined[actorId].actor:teleport(d.defaultCell, d.defaultPos, d.defaultRot)
                    end)
                    if not stat then
                        print("[GoHome]: WARNING! Failed to teleport NPC!?")
                        print(err)
                        print(aux_util.deepToString(data, 4))
                    end
                else
                    print("[GoHome]: WARNING! This actor had no defaults data?!")
                    print(aux_util.deepToString(data, 4))
                end
            end
            routined[actorId] = nil
        end

        -- Populated by the interface
        actorBlacklist = {}
        cellWhitelist = {}
        cellGridWhitelist = {}
        modBlacklist = {}
        noFindHome = {}

        -- Populated by the mod
        actorBlacklistInternal = {}
        actorQuests = {}
        goneHome = {}
        routined = {}
        merchants = {}
        questExceptions = {}
        scriptWhitelist = {}
    end
end

local function isNotLocal(id)
    if routined[id] == nil then
        -- This should never happen under normal circumstances. If the load
        -- order is changed (either by removal, changing order, or some other
        -- means that is not fully understood at this time) that could cause
        -- either the NPC to not exist anymore or the ID to be changed. Doing a
        -- return here at least avoids an error when that happens.
        msg(string.format("WARNING the given actor ID has no data! %s", id))
        return
    end
    routined[id].isLocal = false
end

-- This gets ran when the updateFreq value changes e.g., via the script settings menu when the user changes it
local function updateUpdater()
    stopUpdate()
    stopUpdate = runUpdate()
end

local function losDisable(actor)
    if routined[actor.id].disabledForQuest == false and routined[actor.id].enabled == true and actor.enabled == true then
        msg(string.format("LOS disable on actor: %s", actor.recordId))
        routined[actor.id].enabled = false
        setGoneHome(actor)
        actor.enabled = false
    end
end

local function newRoutined(data, doorExitPos, doorExitRot)
    local r = {
        actor = data.actor,
        defaults = data.defaults,
        disabledForQuest = false,
        enabled = true,
        init = true,
        scheduledDisable = false
    }
    if doorExitPos and doorExitRot then
        r["doorExitPos"] = doorExitPos
        r["doorExitRot"] = doorExitRot
        r["indoors"] = false
    else
        r["noDoor"] = true
    end
    routined[data.actor.id] = r
end

local function setRoutined(data)
    if data.door == nil or data.door.type.destCell == nil then
        msg(string.format("Actor has no door (1): %s", data.actor.recordId))
        newRoutined(data)
        return
    end
    local homeCellDoors = data.door.type.destCell(data.door):getAll(types.Door)
    local doorExitPos
    local doorExitRot
    for _, door in pairs(homeCellDoors) do
        if door.type.destCell == nil or door.type.destCell(door) == nil then
            goto nextDoor
        end
        if door.type.destCell(door).name == data.actor.cell.name then
            doorExitPos = door.type.destPosition(door)
            doorExitRot = door.type.destRotation(door)
        end
        ::nextDoor::
    end
    if doorExitPos == nil or doorExitRot == nil then
        newRoutined(data) --TODO: Does this work? How do we arrive at this code?!
        msg(string.format("Actor has no door (2): %s", data.actor.recordId))
        return
    end
    msg(string.format("Actor is fully routined: %s", data.actor.recordId))
    newRoutined(data, doorExitPos, doorExitRot)
end

-- Reset an NPC back to their default position. Useful when they are
-- mmisbehaving because of navigator shenanigans.
local function teleportMe(data)
    local actorId = data.actor.id
    data.actor:teleport(
        routined[actorId].defaults.defaultCell,
        data.pos,
        routined[actorId].defaults.defaultRot
    )
end

local function tryAgain(actor)
    routined[actor.id] = nil
    onActorActive(actor)
end

local function updateWeatherMonitor(bool)
    if bool == false then
        msg("Stopping weather monitor")
        stopWeatherMonitor()
    else
        msg("Resuming weather monitor")
        stopWeatherMonitor = runWeatherMonitor()
    end
end

local function updateWeatherMonitorFreq()
    msg("Restarting weather monitor to apply new frequency value")
    stopWeatherMonitor()
    stopWeatherMonitor = runWeatherMonitor()
end

-- Engine handoff
return {
    engineHandlers = {
        onActorActive = onActorActive,
        onLoad = onLoad,
        onSave = onSave
    },
    eventHandlers = {
        momw_gh_actorDied = actorDied,
        momw_gh_cellChanged = cellChanged,
        momw_gh_comeOutside = comeOutside,
        momw_gh_disableMe = disableMe,
        momw_gh_enableMe = enableMe,
        momw_gh_factoryResetGlobal = factoryReset,
        momw_gh_global_updateUpdater = updateUpdater,
        momw_gh_isNotLocal = isNotLocal,
        momw_gh_losDisable = losDisable,
        momw_gh_retryEnableMe = retryEnableMe,
        momw_gh_setRoutined = setRoutined,
        momw_gh_showLocalNPCs = showLocalNPCs,
        momw_gh_showNPC = showNPC,
        momw_gh_teleportMe = teleportMe,
        momw_gh_tryAgain = tryAgain,
        momw_gh_tryToGoHome = tryToGoHome,
        momw_gh_updateWeatherMonitor = updateWeatherMonitor,
        momw_gh_updateWeatherMonitorFreq = updateWeatherMonitorFreq,
        momw_gh_useDoor = useDoor
    },
    interfaceName = common.MOD_ID,
    interface = {
        version = common.interfaceVersion,
        ShowAll = showAll,
        GetNPC = getNPC,
        IsActive = function() return not disabled end,
        RegisterActorBlacklist = registerActorBlacklist,
        RegisterGridCellWhitelist = registerGridCellWhitelist,
        RegisterNamedCellWhitelist = registerNamedCellWhitelist,
        RegisterNoFindHome = registerNoFindHome,
        RegisterModBlacklist = registerModBlacklist,
        RegisterQuestExceptions = registerQuestExceptions,
        RegisterScriptWhitelist = registerScriptWhitelist
    }
}
