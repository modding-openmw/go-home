local async = require("openmw.async")
local camera = require("openmw.camera")
local core = require("openmw.core")
local nearby = require("openmw.nearby")
local self = require("openmw.self")
local storage = require("openmw.storage")
local types = require("openmw.types")
local ui = require('openmw.ui')
local util = require("openmw.util")
local I = require("openmw.interfaces")
local common = require("scripts.go-home.common")
local L = core.l10n(common.MOD_ID)
local modSettings = storage.globalSection("SettingsAMod" .. common.MOD_ID)
local timingSettings = storage.globalSection("SettingsCTiming" .. common.MOD_ID)
local factoryResetSettings = storage.globalSection("SettingsZFactoryReset" .. common.MOD_ID)

local REQUIRED_API_REVISION = 69

local errors = {}
if core.API_REVISION < REQUIRED_API_REVISION then
    table.insert(errors, core.l10n(common.MOD_ID)("needNewerOpenMW"))
end

if core.contentFiles.has("NPC_Schedule(beta).esp") then
    table.insert(errors, core.l10n(common.MOD_ID)("dontLoadNPCScheduleEsp"))
end

if not core.contentFiles.has("go-home.omwaddon") then
    table.insert(errors, core.l10n(common.MOD_ID)("needOmwAddon"))
end

if #errors > 0 then
    local errTxt = "ERROR!!!!\nGo Home! could not be started due to errors:"
    for _, err in pairs(errors) do
        errTxt = errTxt .. "\n\n" .. err
    end
    ui.showMessage(errTxt)
    print("!!!!!!!!!!ERRORS!!!!!!!!!!")
    print(string.format("Go Home! had %s errors loading:", #errors))
    for _, err in pairs(errors) do
        print(err)
    end
    error("Could not load Go Home! due to errors!")
end

local veryVerboseMsg = common.veryVerboseMsg

-- Times two cells to account for really big cities like Old Ebonheart
local maxCellsLocal = 2
local teleportPadding = 60
local verticalAxis = util.vector3(0, 0, 1)
local waistOffset = verticalAxis * teleportPadding

I.Settings.registerPage {
    key = common.MOD_ID,
    l10n = common.MOD_ID,
    name = "name",
    description = "description"
}

local function notLocal(actor)
    return (actor.position - self.position):length() >= common.oneCell * maxCellsLocal
end

local function tooClose(actor)
    local minDist = modSettings:get("minDisableDist")
    if minDist == 0 then return false end
    return (actor.position - self.position):length() < minDist
end

-- Big thanks to wazabear for helping me with this code!
local function isCameraFacing(actor)
    local toScreen = camera.worldToViewportVector(actor.position)
    local screenBounds = ui:screenSize()
    return toScreen.x > 0 and toScreen.x < screenBounds.x and toScreen.y > 0 and toScreen.y < screenBounds.y
end

-- This function only recieves actors that are local to the player (see
-- getLocalActors below as well as the corresponding momw_gh_tryToGoHome global event)
local function checkLOS(actor)
    if tooClose(actor) then return end
    if modSettings:get("useRenderingRaycast") then
        nearby.asyncCastRenderingRay(
            async:callback(
                function(res)
                    if res and
                        (res.hitObject == actor
                         or (res.hitObject ~= nil and res.hitObject.type == types.NPC))
                    then
                        return
                    end
                    if res.hitPos then
                        veryVerboseMsg(string.format("Raycast LOS miss with actor: %s", actor.recordId))
                        core.sendGlobalEvent("momw_gh_losDisable", actor)
                    end
            end),
            self.position + waistOffset,
            actor.position + waistOffset)
        return
    end
    if not isCameraFacing(actor) then
        -- The player is looking away from the direction of this NPC
        veryVerboseMsg(string.format("Viewport LOS miss with actor: %s", actor.recordId))
        core.sendGlobalEvent("momw_gh_losDisable", actor)
    end
end

local retryEnableMeDelay = 1
local function retryEnableMeCallback()
	return async:registerTimerCallback(
        "npc_retry_enable_me",
        function(npc)
            if npc.enabled == true then return end
            if notLocal(npc) then
                core.sendGlobalEvent("momw_gh_isNotLocal", npc.id)
                return
            end

            veryVerboseMsg(string.format("Can't enable; player may be looking at actor: %s", npc.recordId))
            core.sendGlobalEvent("momw_gh_retryEnableMe", npc.id)
        end
    )
end
local remcb = retryEnableMeCallback()

local function checkToComeOutside(data)
    if (data.doorExitPos - self.position):length() <= common.oneCell then
        -- Actors within one cell should come outside
        core.sendGlobalEvent("momw_gh_comeOutside", data.npc.id)
    end
end

-- Check an NPC to see if it's in the same cell as the player, enable them if so
local function checkToEnable(npc)
    -- Locals only, please TODO should we just re-enable everyone each day?!
    if notLocal(npc) then
        core.sendGlobalEvent("momw_gh_isNotLocal", npc.id)
        return
    end

    if isCameraFacing(npc) or tooClose(npc) then
        -- The player is looking in the direction of this NPC; retry after a short delay
        async:newSimulationTimer(retryEnableMeDelay, remcb, npc)
    else
        -- The player is looking away from the direction of this NPC
        core.sendGlobalEvent("momw_gh_enableMe", npc)
    end
end

local function getLocalActors()
    local npcs = {}
    for _, actor in pairs(nearby.actors) do
        if actor.type == types.NPC then
            table.insert(npcs, actor)
        end
    end
    core.sendGlobalEvent("momw_gh_tryToGoHome", npcs)
end

local function updateWeatherMonitor(_, key)
    if key == "goHomeForBadWeather" then
        core.sendGlobalEvent("momw_gh_updateWeatherMonitor", modSettings:get("goHomeForBadWeather"))
    elseif key == "weatherMonitorFreq" then
        core.sendGlobalEvent("momw_gh_updateWeatherMonitorFreq")
    end
end
modSettings:subscribe(async:callback(updateWeatherMonitor))

local function weatherMonitor(weather)
    for _, actor in pairs(nearby.actors) do
        if actor.type == types.NPC then
            -- print("sending weather event to an NPC...") TODO very debug
            actor:sendEvent("momw_gh_weatherMonitor", weather)
        end
    end
end

local function updateUpdater(_, key)
    if key == "updateFreq" and tonumber(timingSettings:get("updateFreq")) ~= nil then
        core.sendGlobalEvent("momw_gh_global_updateUpdater")
        for _, actor in pairs(nearby.actors) do
            if actor.type == types.NPC then
                -- print("sending updater event to an NPC...") TODO very debug
                actor:sendEvent("momw_gh_npc_updateUpdater")
            end
        end
    end
end
timingSettings:subscribe(async:callback(updateUpdater))

local initialFactoryReset = factoryResetSettings:get("factoryReset")
if initialFactoryReset == nil then
    initialFactoryReset = false
end
local function factoryResetWatcher(_, key)
    local factoryResetChanged = factoryResetSettings:get("factoryReset") ~= initialFactoryReset
    if factoryResetChanged and key == "factoryReset" and factoryResetSettings:get("factoryReset") == true then
        initialFactoryReset = true
        ui.showMessage(L("factoryResetGo"))
        core.sendGlobalEvent("momw_gh_factoryResetGlobal")
    elseif factoryResetChanged and key == "factoryReset" and factoryResetSettings:get("factoryReset") == false then
        initialFactoryReset = false
        ui.showMessage(L("factoryResetOff"))
        core.sendGlobalEvent("momw_gh_factoryResetGlobal")
    end
end
factoryResetSettings:subscribe(async:callback(factoryResetWatcher))

local initialDisableOnly = modSettings:get("disableOnly")
if initialDisableOnly == nil then
    initialDisableOnly = false
end
local function disableOnlyWatcher(_, key)
    if modSettings:get("disableOnly") ~= initialDisableOnly and key == "disableOnly" then
        ui.showMessage(L("factoryResetAdvised"))
        initialDisableOnly = modSettings:get("disableOnly")
    end
end
modSettings:subscribe(async:callback(disableOnlyWatcher))

local factoryResetRestore = async:registerTimerCallback(
    "factoryResetRestore",
    function()
        core.sendGlobalEvent("momw_gh_factoryResetGlobal", true)
    end
)
local function onLoad(data)
    if data.scriptVersion < common.scriptVersion and factoryResetSettings:get("factoryReset") == false then
        ui.showMessage(L("autoFactoryResetForUpdate"))
        core.sendGlobalEvent("momw_gh_factoryResetGlobal", true)
        async:newSimulationTimer(1, factoryResetRestore)
    end
end

local function onSave()
    return {scriptVersion = common.scriptVersion}
end

local lastCell = self.cell
local wait = 0
local waitMax = 5 -- Seconds
local function onUpdate(dt)
    -- There's no engine-provided "on cell changed" or equivalent so we roll our own.
    -- Every waitMax seconds, if it's daytime, trigger the cellChanged global event
    -- so disabled NPCs can be rechecked.
    wait = wait + dt
    if wait >= waitMax then
        if self.cell ~= lastCell then
            if not common.isDaytime(
                modSettings:get("hourAM"),
                modSettings:get("hourAM"),
                common.gameHour()
            ) then
                return
            end
            lastCell = self.cell
            core.sendGlobalEvent("momw_gh_cellChanged")
        end
        wait = 0
    end
end

local function showLocalNPCs()
    core.sendGlobalEvent(
        "momw_gh_showLocalNPCs",
        {
            cellName = self.cell.name,
            cellGridX = self.cell.gridX,
            cellGridY = self.cell.gridY
        }
    )
end

return {
    engineHandlers = {
        onLoad = onLoad,
        onSave = onSave,
        onUpdate = onUpdate,
    },
    eventHandlers = {
        momw_gh_checkLOS = checkLOS,
        momw_gh_checkToComeOutside = checkToComeOutside,
        momw_gh_checkToEnable = checkToEnable,
        momw_gh_getLocalActors = getLocalActors,
        momw_gh_weatherMonitor = weatherMonitor
    },
    interfaceName = common.MOD_ID,
    interface = {
        version = common.interfaceVersion,
        ShowLocalNPCs = showLocalNPCs
    }
}
