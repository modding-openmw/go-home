local GHLD = require("openmw.interfaces").GoHomeLockingDoors

if not GHLD then
    error("ERROR: Go Home! Locking Doors is not installed!")
end

print("[GoHomeLockingDoors]: Enabling French language support for named cells!")

GHLD.RegisterDestCellBlacklist({"Balmora, maison de Caïus Cosadès"})

GHLD.RegisterNamedCellWhitelist({
        "Ald Rédaynia", -- Technically from modded, or populated by mods
        "Ald Vélothi",
        "Ald'ruhn",
        "Plantation Arvel",
        -- "Bal Isra", --TODO
        "Balmora",
        "Caldéra",
        "Dagon fel",
        "Plantation Dren",
        "Coeurébène",
        "Gnaar mok",
        "Gnisis",
        "Hla oad",
        "Khuul",
        "Maar gan",
        "Molag mar",
        "Longsanglot, Villedieu",
        "Longsanglot, grand bazar",
        "Longsanglot, place Brindisi Dorom",
        "Longsanglot, palais royal : cour",
        "Longsanglot, cour du temple",
        "Pélagiad",
        "Sadrith Mora",
        "Seyda Nihyn",
        "Village Skaal",
        "Suran",
        "Tel Aruhn",
        "Tel branora",
        "Tel Mora",
        "Tel vos",
        "Vivec, l'Arène",
        "Vivec, quartier étranger",
        "Vivec, quartier Hlaalu",
        "Vivec, quartier Rédoran",
        "Vivec, canton de Saint-Délyn",
        "Vivec, canton de Saint-Olms",
        "Vivec, quartier Telvanni",
        "Vivec, quartier du Temple",
        "Vos",
        "Complexe du Serval"
})
