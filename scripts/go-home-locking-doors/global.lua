local async = require("openmw.async")
local core = require("openmw.core")
local common = require("scripts.go-home.common")
local commonData = require("scripts.go-home.commonData")
local MOD_ID = "GoHomeLockingDoors"

local CHECK_INTERVAL = 1
local UPDATE_INTERVAL = 1

local world = require("openmw.world")
local types = require("openmw.types")
local time = require("openmw_aux.time")
local aux_util = require("openmw_aux.util")
local storage = require("openmw.storage")
local modSettings = storage.globalSection("SettingsTime" .. MOD_ID)
local I = require("openmw.interfaces")

local useFrench = core.contentFiles.has("go-home-locking-doors-fr.omwscripts")
if useFrench then
	print(string.format("[%s]: Registering French language support for Morrowind.esm", MOD_ID))
end

local scriptVersion = 1

local cellDoorCount = {}
local seenDoors = {}

local cellGridWhitelist = {}
local cellWhitelist = {}
local destCellBlacklist = {}
local doorBlacklist = {}

I.Settings.registerGroup {
    key = "SettingsTime" .. MOD_ID,
    page = MOD_ID,
    l10n = MOD_ID,
    name = "modSettingsTitle",
    description = "modSettingsDesc",
    permanentStorage = false,
    settings = {
        {
            key = "hourAM",
            name = "hourAM_name",
            description = "hourAM_desc",
            default = common.defaultHourAM,
            min = 1,
            max = 12,
            renderer = "number"
        },
        {
            key = "hourPM",
            name = "hourPM_name",
            description = "hourPM_desc",
            default = common.defaultHourPM,
            min = 12,
            max = 23,
            renderer = "number"
        },
        {
            key = "maxLockChance",
            name = "maxLockChance_name",
            description = "maxLockChance_desc",
            default = 95,
            min = 1,
            max = 100,
            renderer = "number"
        },
        {
            key = "maxLockLevel",
            name = "maxLockLevel_name",
            description = "maxLockLevel_desc",
            default = 75,
            min = 1,
            max = 100,
            renderer = "number"
        },
        {
            key = "businessExtraWeight",
            name = "businessExtraWeight_name",
            description = "businessExtraWeight_desc",
            default = 15,
            min = 1,
            max = 50,
            renderer = "number"
        },
        {
            key = "debugOn",
            name = "debugOn_name",
            description = "debugOn_desc",
            default = false,
            renderer = "checkbox"
        },
        {
            key = "factoryReset",
            name = "factoryReset_name",
            description = "factoryReset_desc",
            default = false,
            renderer = "checkbox"
        }
    }
}

local function factoryReset(enabled)
    if enabled == true then
        print(string.format("[%s]: Factory Reset initiated!", MOD_ID))
        for _, data in pairs(seenDoors) do
            if not data.door:isValid() then goto continue end
            if data.initialState and data.initialState.locked and not data.door.type.isLocked(data.door) then
                data.door.type.lock(data.door, data.initialState.level)
            elseif data.initialState and not data.initialState.locked and data.door.type.isLocked(data.door) then
                data.door.type.unlock(data.door)
            end
            ::continue::
        end
        cellDoorCount = {}
        seenDoors = {}
        cellGridWhitelist = {}
        cellWhitelist = {}
        destCellBlacklist = {}
        doorBlacklist = {}
    end
end

local factoryResetOn = modSettings:get("factoryReset")
local function factoryResetWatcher(_, key)
    local factoryResetChanged = modSettings:get("factoryReset") ~= factoryResetOn
    if factoryResetChanged and key == "factoryReset" and modSettings:get("factoryReset") == true then
        factoryResetOn = true
        world.players[1]:sendEvent("momw_ghld_announceFactoryReset", true)
    elseif factoryResetChanged and key == "factoryReset" and modSettings:get("factoryReset") == false then
        print(string.format("[%s]: Factory Reset discontinued. This mod is now enabled!", MOD_ID))
        factoryResetOn = false
        world.players[1]:sendEvent("momw_ghld_announceFactoryReset", false)
    end
    if factoryResetChanged and factoryResetOn then
        factoryReset(factoryResetOn)
    end
end
modSettings:subscribe(async:callback(factoryResetWatcher))

local debugOn = modSettings:get("debugOn")
local function updateDebugs(_, key)
    if key == "debugOn" then
        debugOn = modSettings:get("debugOn")
    end
end
modSettings:subscribe(async:callback(updateDebugs))

local maxLockChance = modSettings:get("maxLockChance")
local function updateMaxLockChance(_, key)
    if key == "maxLockChance" then
        maxLockChance = modSettings:get("maxLockChance")
    end
end
modSettings:subscribe(async:callback(updateMaxLockChance))

local maxLockLevel = modSettings:get("maxLockLevel")
local function updateMaxLockLevel(_, key)
    if key == "maxLockLevel" then
        maxLockLevel = modSettings:get("maxLockLevel")
    end
end
modSettings:subscribe(async:callback(updateMaxLockLevel))

local businessExtraWeight = modSettings:get("businessExtraWeight")
local function updateBusinessExtraWeight(_, key)
    if key == "businessExtraWeight" then
        businessExtraWeight = modSettings:get("businessExtraWeight")
    end
end
modSettings:subscribe(async:callback(updateBusinessExtraWeight))

local hourAM = modSettings:get("hourAM")
local hourPM = modSettings:get("hourPM")
local function updateHours(_, key)
    if key == "hourAM" then
        hourAM = modSettings:get("hourAM")
    elseif key == "hourPM" then
        hourPM = modSettings:get("hourPM")
    end
end
modSettings:subscribe(async:callback(updateHours))

local function msg(...)
    if debugOn then
        print(string.format("[%s]: %s", MOD_ID, ...))
    end
end

local function registerDestCellBlacklist(cellNames)
    for _, name in pairs(cellNames) do
        msg(string.format("Blacklisting named cell: %s", name))
        destCellBlacklist[name] = true
    end
end

local function registerDoorBlacklist(doorRecordIds)
    for _, id in pairs(doorRecordIds) do
        msg(string.format("Blacklisting door: %s", id))
        doorBlacklist[id] = true
    end
end

local function registerGridCellWhitelist(grids)
    for _, g in pairs(grids) do
        msg(string.format("Whitelisting cell: %s, %s", g.x, g.y))
        local name = common.gridToStr(g.x, g.y)
        cellGridWhitelist[name] = true
        world.players[1]:sendEvent(
            "momw_ghld_receiveCellWhitelists",
            {name = name}
        )
    end
end

local function registerNamedCellWhitelist(cellNames)
    for _, name in pairs(cellNames) do
        msg(string.format("Whitelisting cell: %s", name))
        cellWhitelist[name] = true
        world.players[1]:sendEvent(
            "momw_ghld_receiveCellWhitelists",
            {name = name}
        )
    end
end

local namedCellWhitelist = {}
-- Filter out camps
for _, cell in pairs(commonData.namedCellWhitelistEN) do
    if not string.match(string.lower(cell), "camp") then
        table.insert(namedCellWhitelist, cell)
    end
end
local function registerAllDefaults()
    if core.contentFiles.has("Morrowind.esm") then
        print(string.format("[%s]: Registering support for Morrowind.esm", MOD_ID))
        registerDestCellBlacklist({
                "Balmora, Caius Cosades' House",
                "Sadrith Mora, Wolverine Hall: Mage's Guild"
        })
        registerGridCellWhitelist(commonData.gridCellWhitelist)
        registerNamedCellWhitelist(namedCellWhitelist)
        registerDoorBlacklist({
                "chargen_cabindoor",
                "chargen_ship_trapdoor",
                "chargen customs door",
                "chargen door captain",
                "chargendoorjournal"
        })
    end

    if core.contentFiles.has("TR_Mainland.esm") then
        print(string.format("[%s]: Registering support for TR_Mainland.esm", MOD_ID))
        registerGridCellWhitelist(commonData.trGridCellWhitelist)
        registerNamedCellWhitelist(commonData.trNamedCellWhitelist)
    end

    if core.contentFiles.has("Sky_Main.esm") then
        print(string.format("[%s]: Registering support for Sky_Main.esm", MOD_ID))
        -- https://en.uesp.net/wiki/Project_Tamriel:Skyrim/Places
        registerNamedCellWhitelist(commonData.shotnNamedCellWhitelist)
    end

    if core.contentFiles.has("Cyr_Main.esm") then
        print(string.format("[%s]: Registering support for Cyr_Main.esm", MOD_ID))
        registerNamedCellWhitelist(commonData.pcNamedCellWhitelist)
        registerGridCellWhitelist(commonData.pcGridCellWhitelist)
    end

    if core.contentFiles.has("Cutting Room Floor - Herders.esp") then
        print(string.format("[%s]: Registering support for Cutting Room Floor - Herders.esp", MOD_ID))
        registerGridCellWhitelist(commonData.crfhGridCellWhitelist)
    end

    if core.contentFiles.has("Cutting Room Floor - Ald Redaynia.esp") then
        print(string.format("[%s]: Registering support for Cutting Room Floor - Ald Redaynia.esp", MOD_ID))
        registerNamedCellWhitelist(commonData.crfrNamedCellWhitelist)
    end
end

registerAllDefaults()

local function shouldLock(nearbyDoorCount, objData)
    if objData.initialState.locked then return true end
    local chance = nearbyDoorCount + objData.lockCount
    if objData.business then
        -- Greater chance for a business to lock up
        chance = chance + businessExtraWeight
    end
    if chance > maxLockChance then
         chance = maxLockChance
    end
    return math.random() < chance / 100.0
end

local function lockLevel(nearbyDoorCount, objData)
    if objData.initialState.locked then return objData.initialState.level end
    local level = math.min(nearbyDoorCount + objData.lockCount, maxLockLevel)
    if level > maxLockLevel then
        return maxLockLevel
    end
    return level
end

local function maybeUnlock(obj)
    if seenDoors[obj.id].initialState.locked then return end
    if not obj.type.isLocked(obj) then return end
    -- Random very small chance to not unlock
    -- Much smaller chance for businesses
    local chance = .02
    if seenDoors[obj.id].business then
        chance = .001
    end
    if math.random() < chance then
        msg("NOT unlocking door: " .. obj.recordId .. " (" .. obj.type.destCell(obj).name .. ")")
        return
    end
    obj.type.unlock(obj)
    msg("Unlocked door: " .. obj.recordId .. " (" .. obj.type.destCell(obj).name .. ")")
end

local function newSeen(data)
    local destCell = data.destCell
    local obj = data.obj
    seenDoors[obj.id] = {
        business = data.business,
        cellGrid = {
            x = destCell.gridX,
            y = destCell.gridY
        },
        cellName = data.cellName,
        destCellName = destCell.name,
        door = obj,
        lockCount = 0,
        managed = true,
        initialState = {
            locked = obj.type.isLocked(obj),
            level = obj.type.getLockLevel(obj)
        }
    }
end

local function lockableHome(destCellName)
    local c = string.lower(destCellName)
    if useFrench then
        return string.match(c, "cabane d")
            or string.match(c, "maison d")
            or string.match(c, "maison hlaalu d")
            or string.match(c, " du manoir")
            or string.match(c, "ferme d")
            or string.match(c, "hutte d")
            or string.match(c, "entrepôt d")
            or (string.match(c, "vivec, ")
                and (string.match(c, " nord de ") or string.match(c, " sud de ")))
    end
    return string.match(c, "s' shack")
        or string.match(c, "'s shack")
        or string.match(c, "s' house")
        or string.match(c, "'s house")
        or string.match(c, " manor ")
        or string.match(c, "'s farmhouse")
        or string.match(c, "s' farmhouse")
        or string.match(c, "'s hut")
        or string.match(c, "s' hut")
        or string.match(c, "'s storage")
        or string.match(c, "s' storage")
        or (string.match(c, "vivec, ")
            and (string.match(c, "north-") or string.match(c, "south-")))
end

local function lockableBusiness(destCellName)
    local c = string.lower(destCellName)
    if useFrench then
        return string.match(c, ", tailleur") -- Outfitter & Clothier
            or string.match(c, ", bouquiniste")
            or string.match(c, ", marchand")
            or string.match(c, ", armurier")
            or string.match(c, ", prêteur sur gages")
            or string.match(c, ", quincailler")
            or string.match(c, ", alchimiste")
            or string.match(c, ", enchanteur")
            or string.match(c, ", apothicaire")
            or string.match(c, ", guérisseur")
            or string.match(c, ", mage")
            or string.match(c, ", moine")
            or string.match(c, ", forgeron") -- Smith
    end
    return string.match(c, ": outfitter")
        or string.match(c, " bookseller")
        or string.match(c, " trader")
        or string.match(c, ": clothier")
        or string.match(c, ": armorer")
        or string.match(c, " pawnbroker")
        or string.match(c, ": general merchandise")
        or string.match(c, " alchemist")
        or string.match(c, ": enchanter")
        or string.match(c, ": apothecary")
        or string.match(c, ": healer")
        or string.match(c, ": mage")
        or string.match(c, ": monk")
        or string.match(c, " smith")
end

local function onObjectActive(obj)
    if factoryResetOn then return end
    local unmanaged = {managed = false, door = obj}
    if types.Door.objectIsInstance(obj) and seenDoors[obj.id] == nil then
        local destCell = obj.type.destCell(obj)
        if doorBlacklist[obj.recordId] then
            seenDoors[obj.id] = unmanaged
            return
        end
        if not (cellWhitelist[obj.cell.name]
                or cellGridWhitelist[common.gridToStr(obj.cell.gridX, obj.cell.gridY)]) then
            seenDoors[obj.id] = unmanaged
            return
        end
        if destCell and lockableHome(destCell.name) then
            if destCellBlacklist[destCell.name] then
                msg(string.format("Blacklisted door (destCellBlacklist): %s", obj.recordId))
                seenDoors[obj.id] = unmanaged
            else
                msg(string.format("Found home door: %s (%s)", obj.recordId, destCell.name))
                newSeen({
                        business = false,
                        destCell = destCell,
                        obj = obj,
                })
            end
        elseif destCell and lockableBusiness(destCell.name) then
            if destCellBlacklist[destCell.name] then
                msg(string.format("Blacklisted door (destCellBlacklist): %s", obj.recordId))
                seenDoors[obj.id] = unmanaged
            else
                msg(string.format("Found business door: %s (%s)", obj.recordId, destCell.name))
                newSeen({
                        business = true,
                        destCell = destCell,
                        obj = obj,
                })
            end
        elseif not destCell then
            msg(string.format("Unmanaged door: %s (no destCell)", obj.recordId))
            seenDoors[obj.id] = unmanaged
        end
    end
end

local function onLoad(data)
    if not data then return end
    cellDoorCount = data.cellDoorCount
    for obj, doorData in pairs(data.seenDoors) do
        seenDoors[obj.id] = doorData
        seenDoors[obj.id].door = obj
    end
    -- Force factory reset if scriptVersion is changed
    if data.scriptVersion < scriptVersion then
        factoryReset(true)
    end
end

local function onSave()
    local safeToSaveSeen = {}
    for _, data in pairs(seenDoors) do
        safeToSaveSeen[data.door] = data
    end
    return {
        cellDoorCount = cellDoorCount,
        scriptVersion = scriptVersion,
        seenDoors = safeToSaveSeen
    }
end

local function checkSeenDoors(doorData)
    for _, door in pairs(doorData.doors) do
        local seenData = seenDoors[door.id]
        if seenData and seenData.managed then
            if (doorData.player.position - door.position):length() <= common.oneCell then
                if seenDoors[door.id].lastSeen == nil then
                    seenDoors[door.id].lastSeen = doorData.gameTime
                else
                    if doorData.gameTime - seenDoors[door.id].lastSeen < time.hour then
                        msg("Not checking door, it hasn't been more than an hour since last check: " .. door.recordId)
                        goto nextDoor
                    else
                        -- Hasn't been more than an hour of game time
                        seenDoors[door.id].lastSeen = doorData.gameTime
                    end
                end
                if doorData.daytime then
                    maybeUnlock(door)
                elseif doorData.nighttime then
                    seenDoors[door.id].func = time.runRepeatedly(
                        function()
                            local d = seenDoors[door.id]
                            if door.type.isLocked(door) then
                                msg("Door already locked: " .. door.recordId .. " (" .. d.destCellName .. ")")
                                seenDoors[door.id].func() -- Stop checking
                                seenDoors[door.id].func = nil
                                return
                            end
                            local cellName
                            if door.cell.name == "" then
                                cellName = common.gridToStr(door.cell.gridX, door.cell.gridY)
                            else
                                cellName = door.cell.name
                            end
                            local nearbyCount = cellDoorCount[cellName]
                            if nearbyCount then
                                local should = shouldLock(nearbyCount, d)
                                seenDoors[door.id].lastSeen = doorData.gameTime
                                if should then
                                    door.type.lock(door, lockLevel(nearbyCount, d))
                                    -- Random chance to reset lockCount
                                    if math.random() == 1 then
                                        seenDoors[door.id].lockCount = 0
                                    end
                                    seenDoors[door.id].lockCount = seenDoors[door.id].lockCount + 1
                                    msg("Locked door: " .. door.recordId .. " (" .. d.destCellName .. ")")
                                    seenDoors[door.id].func() -- Stop checking
                                    seenDoors[door.id].func = nil
                                else
                                    msg("Not locking door: " .. door.recordId .. " (" .. d.destCellName .. ")")
                                    seenDoors[door.id].func() -- Stop checking
                                    seenDoors[door.id].func = nil
                                end
                            end
                        end,
                        CHECK_INTERVAL
                    )
                end
            end
        end
        ::nextDoor::
    end
end

local current
local function update()
    if factoryResetOn then return end
    local player = world.players[1]
    if not (player.cell.isExterior or player.cell:hasTag("QuasiExterior")) then return end
    local g = common.gameHour()
    local daytime = common.isDaytime(g, hourAM, hourPM)
    local nighttime = common.isNighttime(g, hourAM, hourPM)
    local changed = false
    if daytime and current ~= "day" then
        current = "day"
        changed = true
    elseif nighttime and current ~= "night" then
        current = "night"
        changed = true
    end
    if changed then
        msg("day-night change detected; currently: " .. current)
        player:sendEvent(
            "momw_ghld_requestLocalDoors",
            {
                gameTime = core.getGameTime(),
                daytime = daytime,
                nighttime = nighttime
            }
        )
    end
end

time.runRepeatedly(
    update, UPDATE_INTERVAL,
    {
        initialDelay = 1 * time.second,
        type = time.SimulationTime
    }
)

-- Events
local function cellChanged(data)
    if factoryResetOn then return end
    local player = world.players[1]
    if not (player.cell.isExterior or player.cell:hasTag("QuasiExterior")) then return end
    local g = common.gameHour()
    local daytime = common.isDaytime(g, hourAM, hourPM)
    local nighttime = common.isNighttime(g, hourAM, hourPM)
    player:sendEvent(
        "momw_ghld_requestLocalDoors",
        {
            gameTime = core.getGameTime(),
            daytime = daytime,
            nighttime = nighttime
        }
    )
    if cellDoorCount[data.cellName] then return end
    cellDoorCount[data.cellName] = data.doorCount
    msg("Counted " .. cellDoorCount[data.cellName] .. " doors for cell: " .. data.cellName)
end

-- Interface functions
local function showAll()
    print("Seen doors:")
    print(aux_util.deepToString(seenDoors, 4))
    print("Cell door counts:")
    print(aux_util.deepToString(cellDoorCount, 4))
end

return {
    engineHandlers = {
        onObjectActive = onObjectActive,
        onSave = onSave,
        onLoad = onLoad
    },
    eventHandlers = {
        momw_ghld_cellChanged = cellChanged,
        momw_ghld_factoryResetGlobal = factoryReset,
        momw_ghld_receiveLocalDoors = checkSeenDoors
    },
    interfaceName = MOD_ID,
    interface = {
        RegisterDestCellBlacklist = registerDestCellBlacklist,
        RegisterDoorBlacklist = registerDoorBlacklist,
        RegisterGridCellWhitelist = registerGridCellWhitelist,
        RegisterNamedCellWhitelist = registerNamedCellWhitelist,
        ShowAll = showAll
    }
}
