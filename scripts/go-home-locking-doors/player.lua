local async = require("openmw.async")
local core = require("openmw.core")
local nearby = require("openmw.nearby")
local self = require("openmw.self")
local time = require("openmw_aux.time")
local ui = require("openmw.ui")
local I = require("openmw.interfaces")
local common = require("scripts.go-home.common")

local MOD_ID = "GoHomeLockingDoors"
local L = core.l10n(MOD_ID)
local SCRIPT_VERSION = 1

local REQUIRED_API_REVISION = 69

local errors = {}
if core.API_REVISION < REQUIRED_API_REVISION then
    table.insert(errors, core.l10n(common.MOD_ID)("needNewerOpenMW"))
end

if core.contentFiles.has("NighttimeDoorLocks-LD 1.1b.esp") then
    table.insert(errors, core.l10n(common.MOD_ID)("dontLoadNighttimeDoorLocksEsp"))
end

if #errors > 0 then
    local errTxt = "ERROR!!!!\nGo Home! could not be started due to errors:"
    for _, err in pairs(errors) do
        errTxt = errTxt .. "\n\n" .. err
    end
    ui.showMessage(errTxt)
    print("!!!!!!!!!!ERRORS!!!!!!!!!!")
    print(string.format("Go Home! Locking Doors had %s errors loading:", #errors))
    for _, err in pairs(errors) do
        print(err)
    end
    error("Could not load Go Home! Locking Doors due to errors!")
end


local CELL_SCAN_INTERVAL = .5

local cellWhitelist = {}
local currentCell
local factoryResetOn = false
local scannedCells = {}

I.Settings.registerPage {
    key = MOD_ID,
    l10n = MOD_ID,
    name = "name",
    description = "description"
}

local function cellScanner()
    if factoryResetOn then return end
    if not (self.cell.isExterior or self.cell:hasTag("QuasiExterior")) then return end
    local cellName
    if self.cell.name == "" then
        cellName = common.gridToStr(self.cell.gridX, self.cell.gridY)
    else
        cellName = self.cell.name
    end
    if cellWhitelist[cellName] and not scannedCells[cellName] and cellName ~= currentCell then
        scannedCells[cellName] = true
        currentCell = cellName
        core.sendGlobalEvent(
            "momw_ghld_cellChanged",
            {
                cellName = cellName,
                doorCount = #nearby.doors
            }
        )
    elseif scannedCells[cellName] and cellName ~= currentCell then
        currentCell = cellName
        core.sendGlobalEvent("momw_ghld_cellChanged", {cellName = cellName})
    end
end

time.runRepeatedly(
    cellScanner, CELL_SCAN_INTERVAL,
    {initialDelay = 1, type = time.SimulationTime}
)

local function announceFactoryReset(enabled)
    if enabled then
        factoryResetOn = true
        ui.showMessage(L("factoryResetOn"), {showInDialogue = false})
    else
        factoryResetOn = false
        ui.showMessage(L("factoryResetOff"), {showInDialogue = false})
    end
end

local function receiveCellWhitelists(data)
    cellWhitelist[data.name] = true
end

local function requestLocalDoors(data)
    core.sendGlobalEvent(
        "momw_ghld_receiveLocalDoors",
        {
            daytime = data.daytime,
            doors = nearby.doors,
            gameTime = data.gameTime,
            nighttime = data.nighttime,
            player = self
        }
    )
end

local function onLoad(data)
    if not data then return end
    if data.scriptVersion == nil or data.scriptVersion < SCRIPT_VERSION then
        ui.showMessage(L("lDAutoFactoryResetForUpdate"))
        core.sendGlobalEvent("momw_ghld_factoryResetGlobal", true)
    end
    data.currentCell = currentCell
    data.factoryResetOn = factoryResetOn
    data.scannedCells = scannedCells
end

local function onSave()
    return {
        currentCell = currentCell,
        factoryResetOn = factoryResetOn,
        scannedCells = scannedCells,
        scriptVersion = SCRIPT_VERSION
    }
end

return {
    engineHandlers = {
        onLoad = onLoad,
        onSave = onSave
    },
    eventHandlers = {
        momw_ghld_announceFactoryReset = announceFactoryReset,
        momw_ghld_receiveCellWhitelists = receiveCellWhitelists,
        momw_ghld_requestLocalDoors = requestLocalDoors
    }
}
