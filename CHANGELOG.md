## Go Home! Changelog

#### 1.14

* [Go Home!] Lowered the maximum possible value for "Minimum disable/enable distance"
* [Go Home!] Fixed a bug that caused a "Factory Reset Required" message to display when loading the mod for the first time (and on an existing save)
* [Locking Doors] Added missing mod metadata TOML file
* [Locking Doors] Now supports locking some doors in Vivec
* [Go Home! and Locking Doors] Fixed support for the French version of Morrowind (thanks lelimule!)
* [Go Home! and Locking Doors] Updated support for Cutting Room Floor's `Cutting Room Floor - Ald Redaynia.esp` plugin

[Download Link](https://gitlab.com/modding-openmw/go-home/-/packages/35591832)

#### 1.13

* A message box is now presented if there's an installation error detected
* Fixed a problem that in some cases caused NPCs to go indoors or come outside when they were already doing so
* Added support for more NPCs that were previously ignored
* Changed handling for the "Sleepers Awake" quest so that affected NPCs can be managed before the quest starts
* [Locking Doors] Tradehouses are no longer locked at night
* [Locking Doors] Added support for the French version of Morrowind (thanks lelimule!)

[Download Link](https://gitlab.com/modding-openmw/go-home/-/packages/35230050)

#### 1.12

* Requires an OpenMW 0.49 RC build!
* Fixed a problem where NPCs wouldn't come back out/be re-enebled (caused by a change in the last release)

[Download Link](https://gitlab.com/modding-openmw/go-home/-/packages/34334208)

#### 1.11

* Added Go Home! Locking Doors - an optional companion mod that will dynamically lock and unlock various doors
* Fixed a problem where some TR NPCs would find the wrong door and try to go home to it
* Added a few more TR NPCs to the "no find home" list
* Only local NPCs are checked each update versus all NPCs in the database

[Download Link](https://gitlab.com/modding-openmw/go-home/-/packages/34096483)

#### 1.10

* Added support for [Project Cyrodiil](https://www.nexusmods.com/morrowind/mods/44922)

[Download Link](https://gitlab.com/modding-openmw/go-home/-/packages/33714749)

#### 1.9

* Fixed a problem that caused issues if your load order is changed after using this mod

[Download Link](https://gitlab.com/modding-openmw/go-home/-/packages/32173118)

#### 1.8

* Fixed incorrect detection of settings changes for "Disable Only" and "Factory Reset" (due to OpenMW-Lua API changes)
* Fixed broken MWScript detection on actors (due to OpenMW-Lua API changes)

[Download Link](https://gitlab.com/modding-openmw/go-home/-/packages/32001525)

#### 1.7

* Fixed some things that broke because of OpenMW API changes (credit: lelimule)
* Updated NPC code to be more compatible with other mods that affect AI (credit: lelimule)
* Fixed a typo in the settings (thanks Half!)

[Download Link](https://gitlab.com/modding-openmw/go-home/-/packages/30344957)

#### 1.6

* Added a `go-home-fr.omwscripts` plugin to enable support for French language cell names (thank you to lelimule for creating this!)
* Added a configurable delay to making an NPC come out side (or be re-enabled, if applicable)
* Optimized handling of whether or not an NPC should come outside if they are indoors
* Fixed race conditions in the NPC scheduling code
* Better handling of Unknown AI states
* Added code to teleport an NPC back to their default position when they get stuck
    * Sometimes when walking home NPCs can get into a state that breaks the navigator (e.g., being _under_ a pathgrid), this feature will zap them back to a known good spot so that they can try to go home again
    * This now happens when the `FixMe()` NPC interface function is ran
* Fixed some Morrowind content exception rules being wrong (Sleepers Awake, specifically)
* Content exceptions will only load if the related plugin is present in the load order
* Added more modded content exceptions

[Download Link](https://gitlab.com/modding-openmw/go-home/-/packages/26534520)

#### 1.5

* Fixed a bug where an NPC's state indicated they were "at home" but they were outside at night time

[Download Link](https://gitlab.com/modding-openmw/go-home/-/packages/25987719)

#### 1.4

* Fixed a bug where NPCs that should be ignored for a quest were disabled
* Added many more modded and Morrowind content exceptions
* Script optimizations and simplication, as well as more handling for special cases (e.g., `Unkown` AI type and more)

[Download Link](https://gitlab.com/modding-openmw/go-home/-/packages/25976386)

#### 1.3

* Added [a note to the README](/#updating-your-load-order-with-this-mod) about changing your load order when using this mod and the risks involved (as well as how to avoid them)
* Added the `FixMe()` console command for the `luas` context
    * Run this on an NPC that might be stuck (e.g., walking into a wall or tree) to put them into their default state for a short period
    * After that expires they will resume their normal schedule
* Added the `ShowLocalNPCs()` console command for the `luap` context
    * It prints global data for nearby NPCs into the console
* Refactored the NPC script to be simpler, fixed many edge cases
    * The only known, unhandled issue at present is the one that relates to NPCs ending up in the `Unknown` AI package
    * Currently the mod can detect when this happens and tries to recover on its own
    * The `FixMe()` command described above is another way to handle this situation
* Increased the minimum range at which we consider an NPC back to their default position by 4x (it had been 25 units, which is a bit too small and caused weird edge case issues)
* Fixed a problem where NPCs with `_` in their record ID would erroneously not find their home
* NPCs won't disable or re-enable if they're within a certain distance from the player (configurable; set to `0` to disable)
* Fix mod blacklisting not working (the engine lowercases all content file names)
* More Morrowind and modded content exceptions
* Added new, conditional exceptions for vanilla Morrowind content that won't apply when using [BCOM](https://www.nexusmods.com/morrowind/mods/49231)

[Download Link](https://gitlab.com/modding-openmw/go-home/-/packages/25357202)

#### 1.2

* **If you're updating mid-save, please be sure that the Factory Reset option is Off before updating!**
* Added the ability to fully restore the default AiWander state for any NPC this mod touches
    * This mod only changes AI for an NPC with `Wander` as their default AI package
* Reverted the previous change to printing NPC info, improved that
* Fixed a possible issue with losing track of NPC AI when they resume being managed by this mod after having not been for a quest
* Fixed a problem that prevented the rare long delay feature from working
* Fixed the max go inside delay setting not being used (it's now used!)

[Download Link](https://gitlab.com/modding-openmw/go-home/-/packages/25145437)

#### 1.1

* Fixed a possible issue with losing track of NPC AI

[Download Link](https://gitlab.com/modding-openmw/go-home/-/packages/24376154)

#### 1.0

* Vastly simplified NPC AI handling
* NPCs can be universally disabled instead of going home in order to avoid one's save file growing due to how the mod reads NPC home cells
* The NPC `i()` interface function now also shows globally-stored data for the given NPC
* Added a `ShowAll()` global interface function which will print all global data to the console (potentially useful for troubleshooting)
* Properly stored initial NPC rotation data
* Fixed possible enable/disable loops due to timer race conditions
* Fixed some issues with the factory reset feature
* The same "is the camera facing them" test is now used by default for trying to disable NPCs as is used for trying to enable them
    * Previously we did an async rendering raycast but this is probably more lightweight and works just as well
    * A rendering raycast is still available for use as an option and can be selected from the script settings menu
* Properly set NPCs back to their default AI when they go inside their homes
* Fixed double disable attempts on NPCs
* Optimized day/night weather checking in the global script
* The mod will advise the player to factory reset with a pop up message when one is needed
* Further refinement of default exceptions for Morrowind content

[Download Link](https://gitlab.com/modding-openmw/go-home/-/packages/24374428)

#### beta3

* Users can execute a "factory reset" from the script settings menu to revert any changes made by this mod and disable it as well
    * The script settings menu can be found at: ESC >> Options >> Scripts >> Go Home! >> Factory Reset
* The mod will error and fail to start if incompatible mods are loaded alongside it
* Enabled support for NPCs in most [Tamriel Rebuilt](https://www.nexusmods.com/morrowind/mods/42145) cities in the tier I-VI locations from [this link](https://en.uesp.net/wiki/Tamriel_Rebuilt:Cities_%26_Towns)
* Enabled support for NPCs in [Skyrim Home Of The Nords](https://www.nexusmods.com/morrowind/mods/44921) in the various cities, towns, and villages [listed on UESP](https://en.uesp.net/wiki/Project_Tamriel:Skyrim/Places)
* Only enable or disable local NPCs without a home
    * Previously the entire internal database of managed NPCs would be checked
* The chance for an NPC to not be disabled is now configurable
* Disabled NPCs will no longer be enabled if the player is looking in their direction
* NPC filtering (should this mod process them) has been made a bit more efficient
    * NPCs are now evaluated only once (unless a factory reset occurs)
* Fixed a disable/enable loop when daytime would arrive
* Fixed a bug where NPCs would become stuck, unable to transition to the right state
* Fixed a bug where disabling "go home for bad weather" when an NPC was home for bad weather would result in them being stuck standing outside their front door
* Fixed bugs relating to OpenMW Lua API changes
    * The weather monitor had broken due to mwscript variables becoming lowercased under the hood
* NPC state is properly reset when they are ignored by this mod for a quest
* Properly handle cases where an NPC has no default AI package set
* Initialized some variables that weren't before
* Refactored the global `onUpdate` code to use `time.runRepeatedly()` and made the interval configurable
* The "bad weather" status for NPCs is now saved which should reduce back and forth when loading a save when the weather is bad (and the builtin delay causes a shuffle)
* NPC status checks are now done via (configurable) timed intervals instead of during `onUpdate` (in both global and NPC scripts)
* Settings have been split into separate groups
* Added a "Very Verbose" log option for extreme log noise
* Further refinement of default exceptions for Morrowind content
* Overall code cleanup

[Download Link](https://gitlab.com/modding-openmw/go-home/-/packages/24109679)

#### beta2

* Disabling NPCs that don't walk home now has the same chance for a delay as the walkers
* When the mod updates it will gracefully reset itself
* Implemented weather detection; NPCs will notice bad weather and go home. Requires the new `go-home.omwaddon` plugin.
    * Several parameters of this feature are configurable, see the script settings menu or the README for more information
* Implemented player LOS detection to try and not disable NPCs while they are being looked at
* NPCs are now deregistered from this mod when they die (if they are registered to begin with)
* Added many more exceptions (noteably: Mournhold exteriors are now supported)
* Always lowercase scripts internally (via the interface)
* Implemented the `RegisterQuestExceptions` interface.

[Download Link](https://gitlab.com/modding-openmw/go-home/-/packages/21750890)

#### beta1

* Initial version of the mod; a beta release.

[Download Link](https://gitlab.com/modding-openmw/go-home/-/packages/21650076)
